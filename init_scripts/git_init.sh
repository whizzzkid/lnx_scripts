#!/bin/bash

# This script initialises Git

# author:  Tukusej's Sirs
# date:    12 February 2020
# version: 1.13

# Dependencies: git ssh bw gpg2

# TODO
# - create a cron job (which would run on startup) that would add SSH keys to the ssh-agent
#   - than replace the `Add SSH keys to the ssh-agent` in here and remove it from `.bashrc`:
#     - check if the cron job exists; if not, create it
# - add SSH keys to:
#   - (Itens GitLab)
# - Purism GitLab;
# - add GPG keys via APIs to:
#   - GitLab;
#   - GitHub;
#   - Gnome GitLab;
#   - Purism GitLab;
#   - (Itens GitLab).


# User-dependant variables
temp='/dev/shm'
real_name='Tukusej’s Sirs'
user_name="tukusejssirs"
user_email="tukusejssirs@protonmail.com"
real_name_itens='Otto Bolyós'
user_name_itens='otto'
user_email_itens='otto@itens.sk'
editor="nano"
git_push_default="simple"  # [ simple | current | matching | upstream | nothing ]; src: http://git-scm.com/docs/git-config

# Check if `git` and `ssh` are installed; if they are not, install them
# TODO (this depends on `pkg_mgrs.sh` bash function)
# if [[ ! -e /usr/bin/git && ! -e /usr/bin/ssh ]]; then
# 	sudo dnf -y install git ssh
# elif [[ ! -e /usr/bin/git ]]; then
# 	sudo dnf -y install git
# elif [[ ! -e /usr/bin/ssh ]]; then
# 	sudo dnf -y install openssh
# fi

# if [ "$(grep -Po '^ts(?=:)' /etc/passwd)" = 'ts' ]; then
# 	my_user='ts'
# 	my_user_git_local="$(grep -Po 'XDG_GIT_LOCAL_DIR="\K.*(?=")' /home/${my_user}/.config/user-dirs.dirs | sed "s#\$HOME#/home/$my_user#")"
# elif [[ "$(grep -Po '^otto(?=:)' /etc/passwd)" ]]; then
# 	my_user='otto'
# 	my_user_git_local="$(grep -Po 'XDG_GIT_LOCAL_DIR="\K.*(?=")' /home/${my_user}/.config/user-dirs.dirs | sed "s#\$HOME#/home/$my_user#")"
# else
# 	my_user="$USER"
# 	my_user_git_local="$(grep -Po 'XDG_GIT_LOCAL_DIR="\K.*(?=")' ${HOME}/.config/user-dirs.dirs | sed "s#\$HOME#/home/$my_user#")"
# fi

# Git global configuration
git config --global user.name "$real_name"
git config --global user.email $user_email
git config --global push.default $git_push_default
git config --global core.editor $editor
git config --global color.ui auto
git config --global gpg.program gpg2
git config --global commit.gpgsign true

# Git Itens configuration
mkdir -p "${XDG_GIT_DIR}/itens"
echo -e "[user]\n\tname = $real_name_itens\n\temail = $user_email_itens" > "${XDG_GIT_DIR}/itens/.gitconfig"
echo -e "[includeIf \"gitdir:${XDG_GIT_DIR}/itens/\"]\n\tpath = ${XDG_GIT_DIR}/itens/.gitconfig" >> ${HOME}/.gitconfig

# Add the servers to SSH known hosts
ssh-keyscan -t ecdsa-sha2-nistp256 gitlab.com 2>/dev/null >> ${HOME}/.ssh/known_hosts
ssh-keyscan -p 5222 -t ecdsa-sha2-nistp256 git.itens.sk 2>/dev/null >> ${HOME}/.ssh/known_hosts
ssh-keyscan -t ssh-rsa github.com 2>/dev/null >> ${HOME}/.ssh/known_hosts
ssh-keyscan --t ssh-rsa bitbucket.org 2>/dev/null >> ${HOME}/.ssh/known_hosts
ssh-keyscan -t ecdsa-sha2-nistp256 gitlab.gnome.org 2>/dev/null >> ${HOME}/.ssh/known_hosts
ssh-keyscan -t ecdsa-sha2-nistp256 notabug.org 2>/dev/null >> ${HOME}/.ssh/known_hosts

# Generate SSH key for GitLab
ssh-keygen -t ed25519 -a 100 -f ${HOME}/.ssh/gitlab_$(whoami)@${HOSTNAME} -P ""
chmod 600 ${HOME}/.ssh/gitlab_$(whoami)@${HOSTNAME}
# Note: There must be hostname (domain/URL) after `Host`
echo -e "Host gitlab.com\n     Hostname gitlab.com\n     IdentityFile ${HOME}/.ssh/gitlab_$(whoami)@${HOSTNAME}" >> ${HOME}/.ssh/config
echo "GitLab SSH key created."
token=$(node /git/others/bw/build/bw.js get item "71a20424-7c68-4506-8ee1-aaf900d4d4d0" --session $BW_SESSION | jq -r .notes)
key=$(cat ${HOME}/.ssh/gitlab_$(whoami)@${HOSTNAME}.pub)
title="$(whoami)@${HOSTNAME}"
curl -s --data-urlencode "key=$key" --data-urlencode "title=$title" https://gitlab.com/api/v4/user/keys?private_token=$token
echo "GitLab SSH key added to GitLab."

# Generate GPG key for GitLab
# TODO: This does not work with gnupg2 v2.0.22, but works with v2.2.9
# TODO: Manual generation using gnupg2 v2.0.22 on Forpsi VPS, has not enough entropy. Run `dd if=/dev/sda of=/dev/zero` in background before the generation and kill the process after generation is complete
echo -e "Key-Type: 1\nKey-Length: 4096\nSubkey-Type: 1\nSubkey-Length: 4096\nName-Real: $real_name\nName-Email: $user_email\nExpire-Date: 0" > "$temp/gpg_key_data.txt"
# echo -e "\n%no-ask-passphrase\nKey-Type: 1\nKey-Length: 4096\nSubkey-Type: 1\nSubkey-Length: 4096\nName-Real: $real_name\nName-Email: $user_email\nExpire-Date: 0\n%commit\n%echo done" > "$temp/gpg_key_data.txt"
gpg2 --batch --gen-key "$temp/gpg_key_data.txt"
echo "GitLab GPG key created."
rm "$temp/gpg_key_data.txt"
gpg_key_id=$(gpg2 --list-public-keys --keyid-format LONG $user_email | grep -Po "^pub\s*[^/]*/\K[^\s]*"  | tail -1)
gpg_key=$(gpg2 --armor --export $gpg_key_id)
curl --data-urlencode "key=$gpg_key" "https://gitlab.com/api/v4/user/gpg_keys?private_token=$token" &>/dev/null
git config --global user.signingkey $gpg_key_id
echo "GitLab GPG key added to GitLab."

# Generate SSH key for Itens GitLab
ssh-keygen -t ed25519 -a 100 -f ${HOME}/.ssh/itens_gitlab_$(whoami)@${HOSTNAME} -P ""
chmod 600 ${HOME}/.ssh/itens_gitlab_$(whoami)@${HOSTNAME}
# Note: There must be hostname (domain/URL) after `Host`
echo -e "Host git.itens.sk\n     Hostname git.itens.sk\n     IdentityFile ${HOME}/.ssh/itens_gitlab_$(whoami)@${HOSTNAME}" >> ${HOME}/.ssh/config
echo "Itens GitLab SSH key created."
token=$(node /git/others/bw/build/bw.js get item "1d62f78b-fab0-4cef-87e8-aafd01559445" --session $BW_SESSION | jq -r .notes)
key=$(cat ${HOME}/.ssh/itens_gitlab_$(whoami)@${HOSTNAME}.pub)
title="$(whoami)@${HOSTNAME}"
curl --data-urlencode "key=$key" --data-urlencode "title=$title" https://git.itens.sk/api/v4/user/keys?private_token=$token
echo "Itens GitLab SSH key added to Itens GitLab."

# Generate GPG key for Itens GitLab
echo -e "Key-Type: 1\nKey-Length: 4096\nSubkey-Type: 1\nSubkey-Length: 4096\nName-Real: $real_name_itens\nName-Email: $user_email_itens\n%no-protection\nExpire-Date: 0" > "$temp/gpg_key_data.txt"
gpg2 --batch --gen-key "$temp/gpg_key_data.txt"
echo "Itens GitLab GPG key created."
rm "$temp/gpg_key_data.txt"
gpg_key_id=$(gpg2 --list-public-keys --keyid-format LONG $user_email_itens | grep -Po "^pub\s*[^/]*/\K[^\s]*"  | tail -1)
gpg_key=$(gpg2 --armor --export $gpg_key_id)
curl --data-urlencode "key=$gpg_key" https://git.itens.sk/api/v4/user/gpg_keys?private_token=$token &>/dev/null
if [ "${XDG_GIT_DIR}/itens/.gitconfig" ]; then
	sed -i '/signingkey/d' "${XDG_GIT_DIR}/itens/.gitconfig"
fi
echo -e "\tsigningkey = $gpg_key_id" >> "${XDG_GIT_DIR}/itens/.gitconfig"
echo "Itens GitLab GPG key added to Itens GitLab."

# Generate SSH key for GitHub
ssh-keygen -t ed25519 -a 100 -f ${HOME}/.ssh/github_$(whoami)@${HOSTNAME} -P ""
chmod 600 ${HOME}/.ssh/github_$(whoami)@${HOSTNAME}
echo -e "Host github.com\n     Hostname github.com\n     IdentityFile ${HOME}/.ssh/github_$(whoami)@${HOSTNAME}" >> ${HOME}/.ssh/config
echo "GitHub SSH key created."
token=$(node /git/others/bw/build/bw.js get item "89b5923d-b1e1-4d5a-95f5-aaf900d49efc" --session $BW_SESSION | jq -r .notes)
key=$(cat ${HOME}/.ssh/github_$(whoami)@${HOSTNAME}.pub)
title="$(whoami)@${HOSTNAME}"
json=$(printf '{"title": "%s", "key": "%s"}' "$title" "$key")
curl -s -d "$json" "https://api.github.com/user/keys?access_token=$token"
echo "GitHub SSH key added to GitHub."

# Generate SSH key for Bitbucket
ssh-keygen -t ed25519 -a 100 -f ${HOME}/.ssh/bitbucket_$(whoami)@${HOSTNAME} -P ""
chmod 600 ${HOME}/.ssh/bitbucket_$(whoami)@${HOSTNAME}
echo -e "Host bitbucket.org\n     IdentityFile ${HOME}/.ssh/bitbucket_$(whoami)@${HOSTNAME}" >> ${HOME}/.ssh/config

# Generate ssh key for NotABug
ssh-keygen -t ed25519 -a 100 -f ${HOME}/.ssh/notabug_$(whoami)@${HOSTNAME} -P ""
chmod 600 ${HOME}/.ssh/notabug_$(whoami)@${HOSTNAME}
echo -e "Host notabug.org\n     IdentityFile ${HOME}/.ssh/notabug_$(whoami)@${HOSTNAME}" >> ${HOME}/.ssh/config

# Generate ssh key for GNOME GitLab
ssh-keygen -t ed25519 -a 100 -f ${HOME}/.ssh/gnome_gitlab_$(whoami)@${HOSTNAME} -P ""
chmod 600 ${HOME}/.ssh/gnome_gitlab_$(whoami)@${HOSTNAME}
echo -e "Host gitlab.gnome.org\n     Hostname gitlab.gnome.org\n     IdentityFile ${HOME}/.ssh/gnome_gitlab_$(whoami)@${HOSTNAME}" >> ${HOME}/.ssh/config
echo "Gnome GitLab SSH key created."
token=$(node /git/others/bw/build/bw.js get item "1d852b54-9cef-42c8-8712-aaf90101f56d" --session $BW_SESSION | jq -r .notes)
key=$(cat ${HOME}/.ssh/gnome_gitlab_$(whoami)@${HOSTNAME}.pub)
title="$(whoami)@${HOSTNAME}"
curl --data-urlencode "key=$key" --data-urlencode "title=$title" https://gitlab.gnome.org/api/v4/user/keys?private_token=$token
echo "Gnome GitLab SSH key added to GitHub."

# Generate GPG key for Gnome GitLab
# TODO: HOW DO I SEPARATE THE GNOME REPOS FROM OTHERS? USING ${XDG_GIT_LOCAL_DIR}/{C10N,OTHERS}/GNOME?
# echo -e "Key-Type: 1\nKey-Length: 4096\nSubkey-Type: 1\nSubkey-Length: 4096\nName-Real: $real_name\nName-Email: $user_email\n%no-protection\nExpire-Date: 0" > "$temp/gpg_key_data.txt"
# gpg2 --batch --gen-key "$temp/gpg_key_data.txt"
# echo "Gnome GitLab GPG key created."
# rm "$temp/gpg_key_data.txt"
# gpg_key_id=$(gpg2 --list-public-keys --keyid-format LONG $user_email | grep -Po "^pub\s*[^/]*/\K[^\s]*"  | tail -1)
# gpg_key=$(gpg2 --armor --export $gpg_key_id)
# curl --data-urlencode "key=$gpg_key" https://gitlab.gnome.com/api/v4/user/gpg_keys?private_token=$token &>/dev/null
# if [ "${XDG_GIT_DIR}/gnome/.gitconfig" ]; then
# 	sed -i '/signingkey/d' "${my_user_git_local}/gnome/.gitconfig"
# fi
# echo "\tsigningkey = $gpg_key_id" >> "${XDG_GIT_LOCAL_DIR}/gnome/.gitconfig"
# echo "Gnome GitLab GPG key added to Gnome GitLab."

cat > ${HOME}/ks-todo_init.sh << EOF
#!/bin/bash
xclip -r -selection c ${HOME}/.ssh/bitbucket_$(whoami)@${HOSTNAME}.pub
firefox https://id.atlassian.com/login?continue=https://bitbucket.org/account/user/tukusejssirs/ssh-keys &>/dev/null&
echo "BitBucket SSH key copied."
read

xclip -r -selection c ${HOME}/.ssh/notabug_$(whoami)@${HOSTNAME}.pub
firefox https://notabug.org/user/settings/ssh &>/dev/null&
echo "NotABug SSH key copied."
read
EOF

# `chmod` ~/.ssh/config
chmod 600 ${HOME}/.ssh/config

# Reload `gpg-agent` to make the new GPG keys accessible
killall gpg-agent && gpg-agent --daemon --use-standard-socket --pinentry-program /usr/local/bin/pinentry

# It might be also required to run this
# src: https://stackoverflow.com/a/55993078/3408342
export GPG_TTY=$(tty)

# Add SSH keys to the ssh-agent
killall ssh-agent && eval $(ssh-agent -s) &> /dev/null
for n in $(ls ${HOME}/.ssh/*.pub 2>/dev/null | sed 's/\.pub//g' -); do
	ssh-add $n &> /dev/null
done