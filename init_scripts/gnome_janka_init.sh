#!/bin/bash

# This scripts sets my Gnome 3 preferences

# author:  Tukusej's Sirs
# date:    7 January 2020
# version: 0.10


# Set up `gnome-terminal` colours
# src: https://askubuntu.com/a/733202
# src: https://superuser.com/questions/872397/how-to-add-a-pre-made-profile-to-gnome-terminal
profile=$(gsettings get org.gnome.Terminal.ProfilesList default | sed "s/'//g" -)
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" palette "['rgb(0,0,0)', 'rgb(170,0,0)', 'rgb(0,170,0)', 'rgb(170,85,0)', 'rgb(17,17,170)', 'rgb(170,0,170)', 'rgb(0,170,170)', 'rgb(170,170,170)', 'rgb(85,85,85)', 'rgb(255,85,85)', 'rgb(85,255,85)', 'rgb(255,255,85)', 'rgb(85,85,255)', 'rgb(255,85,255)', 'rgb(85,255,255)', 'rgb(255,255,255)']"
gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'dark'
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" audible-bell false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" use-theme-colors false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" background-color '#000000'
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" use-system-font false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" title "$PWD"
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" audible-bell false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" encoding 'UTF-8'
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" bold-color-same-as-fg true
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" login-shell false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" font 'Ubuntu Mono 13'
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" bold-is-bright true
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" scroll-on-output false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" use-transparent-background false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" scroll-on-keystroke true
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" rewrap-on-resize true
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" text-blink-mode never
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" foreground-color '#00ff00'

# `gedit` settings
gsettings set org.gnome.gedit.preferences.editor scheme 'cobalt'

# Disable lock screen and screensaver
gsettings set org.gnome.desktop.lockdown disable-lock-screen true
gsettings set org.gnome.desktop.screensaver lock-enabled false
gsettings set org.gnome.desktop.screensaver idle-activation-enabled false

# Desktop and screensaver background
gsettings set org.gnome.desktop.background primary-color "#000000"
gsettings set org.gnome.desktop.background secondary-color "#000000"
gsettings set org.gnome.desktop.background color-shading-type "solid"
gsettings set org.gnome.desktop.background picture-uri ''   # Choose a picture
gsettings set org.gnome.desktop.screensaver picture-uri ''  # Choose a picture
gsettings set org.gnome.desktop.screensaver primary-color "#000000"
gsettings set org.gnome.desktop.screensaver secondary-color "#000000"
gsettings set org.gnome.desktop.screensaver color-shading-type "solid"

# This turns off auto-brightness
gsettings set org.gnome.settings-daemon.plugins.power ambient-enabled false

# Disable middle-button paste
gsettings set org.gnome.desktop.interface gtk-enable-primary-paste 'false'

# Disable attaching modal dialogues
gsettings set org.gnome.shell.overrides attach-modal-dialogs false

# Disable dimming screen and turning it off
gsettings set org.gnome.settings-daemon.plugins.power idle-dim false
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'       # blank screen
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-type 'nothing'  # blank screen

# Disable location
gsettings set org.gnome.system.location enabled false

# Screenshot save directory
# TODO
# gsettings set org.gnome.gnome-screenshot auto-save-directory "file:///home/$USER/grcs/pics/screenshots/"
# gsettings set org.gnome.gnome-screenshot last-save-directory "file://$XDG_SCREENSHOTS_DIR"

# Configure keyboard shortcuts
gshort "Brightness up" 'gdbus call --session --dest org.gnome.SettingsDaemon.Power --object-path /org/gnome/SettingsDaemon/Power --method org.gnome.SettingsDaemon.Power.Screen.StepUp' "<Alt><Super>Up"  # TODO: set and check if it works with F7
gshort "Brightness down" 'gdbus call --session --dest org.gnome.SettingsDaemon.Power --object-path /org/gnome/SettingsDaemon/Power --method org.gnome.SettingsDaemon.Power.Screen.StepDown' "<Alt><Super>Down"  # TODO: set and check if it works with F8
gshort "gnome-terminal" 'gnome-terminal --geometry=1000x400' "<Super>t"
gshort "Mikrotik WinBox" "wine /home/ts/.prgs/winbox/winbox_v3.19.exe" "<Super>w"
gsettings set org.gnome.settings-daemon.plugins.media-keys home '<Super>e'
gsettings set org.gnome.desktop.wm.keybindings show-desktop "['<Super>d']"
gsettings set org.gnome.desktop.wm.keybindings minimize "['<Super>Down']"
gsettings set org.gnome.desktop.wm.keybindings unmaximize "['<Shift><Super>Down']"

# Additional keyboard layout options
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:internal_nocancel', 'grp:shifts_toggle']"
gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'sk'), ('xkb', 'gb')]"

# Remove apps I don't use
# TODO: rather don’t install packages if they should be removed afterwards
sudo dnf -y remove gnome-weather

# Configure Gnome 3 extensions
# TODO: update this
# sudo rm -rf /usr/share/gnome-shell/extensions && sudo ln -s ${XDG_GIT_DIR}/lnx_scripts/gnome/ext/global /usr/share/gnome-shell/extensions
# rm -rf ${HOME}/.local/share/gnome-shell/extensions && sudo ln -s ${XDG_GIT_DIR}/lnx_scripts/gnome/ext/local ${HOME}/.local/share/gnome-shell/extensions
# gsettings set org.gnome.shell enabled-extensions "['drive-menu@gnome-shell-extensions.gcampax.github.com', 'openweather-extension@jenslody.de', 'extensions@abteil.org', 'status-area-horizontal-spacing@mathematical.coffee.gmail.com', 'noannoyance@sindex.com', 'disconnect-wifi@kgshank.net', 'hide-legacy-tray@shell-extensions.jonnylamb.com', 'advanced-settings-in-usermenu@nuware.ru', 'TopIcons@phocean.net', 'maximus-two@wilfinitlike.gmail.com', 'maximus-three@daman.4880.gmail.com', 'gnome-shell-extension-maximized-by-default@axe1.github.com', 'remove-dropdown-arrows@mpdeimos.com', 'user-theme@gnome-shell-extensions.gcampax.github.com', 'activities-config@nls1729', 'gsconnect@andyholmes.github.io']"

# Create `autostart` folder
mkdir -p ${HOME}/.config/autostart

# Disable suspension after laptop lid is closed
# Dependency: requires `gnome-tweaks` (or at least `gnome-tweak-tool-lid-inhibitor script)
# TODO: make this `gnome-tweaks`-independent
echo -e "[Desktop Entry]\nType=Application\nName=ignore-lid-switch-tweak\nExec=/usr/libexec/gnome-tweak-tool-lid-inhibitor" > ${HOME}/.config/autostart/ignore-lid-switch-tweak.desktop

# Disable touchpad (but keep the trackpoint enabled)
gsettings set org.gnome.desktop.peripherals.touchpad send-events 'disabled'

# Show week number in the panel calendar
gsettings set org.gnome.desktop.calendar show-weekdate 'true'


# org.gnome.online-accounts
# org.gnome.Contacts
# org.gnome.desktop.datetime
# org.gnome.desktop.default-applications
# org.gnome.desktop.default-applications.office
# org.gnome.desktop.default-applications.office.calendar
# org.gnome.desktop.default-applications.office.tasks
# org.gnome.desktop.default-applications.terminal
# org.gnome.desktop.interface
# org.gnome.desktop.lockdown
# org.gnome.desktop.media-handling
# org.gnome.desktop.notifications
# org.gnome.desktop.notifications.application:/
# org.gnome.GWeather
# org.gnome.Weather.Application
# org.gnome.packagekit
# org.gnome.seahorse
# org.gnome.seahorse.manager
# org.gnome.seahorse.window:/
# org.gnome.settings-daemon.plugins.account
# org.gnome.settings-daemon.plugins.color
# org.gnome.settings-daemon.plugins.housekeeping
# org.gnome.settings-daemon.plugins.media-keys
# org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/
# org.gnome.settings-daemon.plugins.xsettings
# org.gnome.shell
# org.gnome.shell.keybindings
# org.gnome.shell.keyboard
# org.gnome.shell.overrides
# org.gnome.eog[.*]  # set the background
# org.gnome.software