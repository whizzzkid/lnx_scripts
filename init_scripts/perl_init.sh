sudo yum -y install perl-CPAN perl-DBD-Pg

echo -e "The following command requires some manual input:"
echo -e "    yes (automatic)"
echo -e "    yes (local::lib)"
echo -e "    yes (auto choose CPAN mirror)"
echo -e "    yes (append paths to .bashrc)"
echo -e "    exit (to exit CPAN shell"
echo -e "yes\nyesnyes\nyes\nexit" | perl -MCPAN -e shell  # This should be okay (in regard to the manual input), but needs to be tested

# Install psql DBD
cpan install DBD::Pg

# Install mysql DBD
#cpan install DBD::mysql