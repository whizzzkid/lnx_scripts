#!/bin/bash

# This scripts sets my Gnome 3 preferences

# author:  Tukusej's Sirs
# date:    25 January 2020
# version: 0.11


# Set up `gnome-terminal` colours
# src: https://askubuntu.com/a/733202
# src: https://superuser.com/questions/872397/how-to-add-a-pre-made-profile-to-gnome-terminal
profile=$(gsettings get org.gnome.Terminal.ProfilesList default | sed "s/'//g" -)
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" palette "['rgb(0,0,0)', 'rgb(170,0,0)', 'rgb(0,170,0)', 'rgb(170,85,0)', 'rgb(17,17,170)', 'rgb(170,0,170)', 'rgb(0,170,170)', 'rgb(170,170,170)', 'rgb(85,85,85)', 'rgb(255,85,85)', 'rgb(85,255,85)', 'rgb(255,255,85)', 'rgb(85,85,255)', 'rgb(255,85,255)', 'rgb(85,255,255)', 'rgb(255,255,255)']"
gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'dark'
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" audible-bell false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" use-theme-colors false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" background-color '#000000'
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" use-system-font false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" title "$PWD"
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" audible-bell false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" encoding 'UTF-8'
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" bold-color-same-as-fg true
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" login-shell false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" font 'Ubuntu Mono 13'
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" bold-is-bright true
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" scroll-on-output false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" use-transparent-background false
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" scroll-on-keystroke true
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" rewrap-on-resize true
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" text-blink-mode never
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" foreground-color '#00ff00'

# `gedit` settings
gsettings set org.gnome.gedit.preferences.editor scheme 'cobalt'

# Disable lock screen and screensaver
gsettings set org.gnome.desktop.lockdown disable-lock-screen true
gsettings set org.gnome.desktop.screensaver lock-enabled false
gsettings set org.gnome.desktop.screensaver idle-activation-enabled false

# Desktop and screensaver background
gsettings set org.gnome.desktop.background primary-color "#000000"
gsettings set org.gnome.desktop.background secondary-color "#000000"
gsettings set org.gnome.desktop.background color-shading-type "solid"
gsettings set org.gnome.desktop.background picture-uri ''   # Choose a picture
gsettings set org.gnome.desktop.screensaver picture-uri ''  # Choose a picture
gsettings set org.gnome.desktop.screensaver primary-color "#000000"
gsettings set org.gnome.desktop.screensaver secondary-color "#000000"
gsettings set org.gnome.desktop.screensaver color-shading-type "solid"

# This turns off auto-brightness
gsettings set org.gnome.settings-daemon.plugins.power ambient-enabled false

# Disable middle-button paste
gsettings set org.gnome.desktop.interface gtk-enable-primary-paste 'false'

# Disable attaching modal dialogues
gsettings set org.gnome.shell.overrides attach-modal-dialogs false

# Disable dimming screen and turning it off
gsettings set org.gnome.settings-daemon.plugins.power idle-dim false
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'       # blank screen
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-type 'nothing'  # blank screen

# Disable location
gsettings set org.gnome.system.location enabled false

# Screenshot save directory
gsettings set org.gnome.gnome-screenshot auto-save-directory "file:///home/$USER/grcs/pics/screenshots/"
gsettings set org.gnome.gnome-screenshot last-save-directory "file://$XDG_SCREENSHOTS_DIR"

# Configure keyboard shortcuts
gshort "Brightness up" 'gdbus call --session --dest org.gnome.SettingsDaemon.Power --object-path /org/gnome/SettingsDaemon/Power --method org.gnome.SettingsDaemon.Power.Screen.StepUp' "<Alt><Super>Up"
gshort "Brightness down" 'gdbus call --session --dest org.gnome.SettingsDaemon.Power --object-path /org/gnome/SettingsDaemon/Power --method org.gnome.SettingsDaemon.Power.Screen.StepDown' "<Alt><Super>Down"
gshort "gnome-terminal" 'gnome-terminal --geometry=1000x400' "<Super>t"
gshort "Remmina Dhollandia" 'bash -c "remmina -c ~/.remmina/1559858372261.remmina"' "<Super>r"
gshort "Mikrotik WinBox" "wine /home/ts/.prgs/winbox/winbox_v3.19.exe" "<Super>w"
gsettings set org.gnome.settings-daemon.plugins.media-keys home '<Super>e'
gsettings set org.gnome.desktop.wm.keybindings show-desktop "['<Super>d']"
gsettings set org.gnome.desktop.wm.keybindings minimize "['<Super>Down']"
gsettings set org.gnome.desktop.wm.keybindings unmaximize "['<Shift><Super>Down']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot 'XF86MyComputer'  # Screenshot by Folder icon (rightmost key in the top row)

# As from GNOME 3.8 on, the screenshot location (`auto-save-directory` and `last-save-direcotry`) are not respected anymore, we need to disable these 6 shortcuts and use custom ones instead
gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot ''
gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot-clip ''
gsettings set org.gnome.settings-daemon.plugins.media-keys window-screenshot ''
gsettings set org.gnome.settings-daemon.plugins.media-keys window-screenshot-clip ''
gsettings set org.gnome.settings-daemon.plugins.media-keys area-screenshot-clip ''
gsettings set org.gnome.settings-daemon.plugins.media-keys area-screenshot ''
gshort "Screenshot of area" 'gnome-screenshot -a' "<Shift>MyComputer"
gshort "Screenshot clip of area" 'gnome-screenshot -a -c' "<Primary><Shift>MyComputer"
gshort "Screenshot" 'gnome-screenshot' "MyComputer"
gshort "Screenshot clip" 'gnome-screenshot -c' "<Primary>MyComputer"
gshort "Screenshot clip of area" 'gnome-screenshot -a -c' "<Primary><Shift>MyComputer"
gshort "Screenshot clip of window" 'gnome-screenshot -w -c' "<Primary><Alt>MyComputer"

gsettings set org.gnome.settings-daemon.plugins.media-keys volume-mute '<Shift><Super>M'
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-down '<Shift><Super>Page_Down'
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-up '<Shift><Super>Page_Up'

# Additional keyboard layout options
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:internal_nocancel', 'grp:shifts_toggle']"
gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'gb'), ('xkb', 'sk+qwerty'), ('xkb', 'il+biblical'), ('xkb', 'gr+polytonic')]"

# Multimedia key codes: http://wiki.linuxquestions.org/wiki/XF86_keyboard_symbols
# TODO: F4 still does not work
# TODO: add this to `.bashrc`
xmodmap -pke > ~/.Xmodmap
xmodmap -e "keycode 107 = Menu"  # PrtSc   → Menu
xmodmap -e "keycode 121 = F1"    # Mute    → F1    # 'XF86AudioMute'
xmodmap -e "keycode 122 = F2"    # Vol-    → F2    # 'XF86AudioLowerVolume'
xmodmap -e "keycode 123 = F3"    # Vol+    → F3    # 'XF86AudioRaiseVolume'
xmodmap -e "keycode 124 = F4"    # MuteMic → F4    # 'XF86AudioMicMute'
xmodmap -pke > ~/.Xmodmap

# Create `autostart` folder
mkdir -p ${HOME}/.config/autostart

# Configure start-up apps
# firefox (does not start maximised)
echo -e "[Desktop Entry]\nType=Application\nEncoding=UTF-8\nName=Firefox\nComment=Internet browser\nExec=/opt/bin/ff_nightly\nIcon=/opt/ff_nightly/browser/chrome/icons/default/default128.png\nTerminal=false" > ${HOME}/.config/autostart/firefox.desktop

# bitwarden (not working yet; there's a runtime error)
echo -e "[Desktop Entry]\nType=Application\nVersion=1.0\nName=Bitwarden\nComment=Password manager\nTryExec=/opt/bitwarden/bitwarden-x86_64.appimage --no-sandbox\nExec=/opt/bitwarden/bitwarden-x86_64.appimage --no-sandbox\nTerminal=false\nStartupNotify=false\nHidden=true" > ${HOME}/.config/autostart/bitwarden.desktop

# mailspring
echo -e "[Desktop Entry]\nType=Application\nEncoding=UTF-8\nName=Mailspring\nComment=Email client\nExec=/usr/bin/mailspring -b\nIcon=mailspring\nTerminal=false" > ${HOME}/.config/autostart/mailspring.desktop

# telegram (not working yet)
echo -e "[Desktop Entry]\nType=Application\nVersion=1.0\nName=Telegram\nComment=Chat client\nTryExec=${XDG_PROG_BIN_DIR}/telegram/Telegram -startintray\nExec=${XDG_PROG_BIN_DIR}/telegram/Telegram -startintray\nTerminal=false\nStartupNotify=false\nHidden=true" > ${HOME}/.config/autostart/telegram.desktop

# subl
echo -e "[Desktop Entry]\nType=Application\nVersion=1.0\nName=Sublime Text 3\nComment=Text editor\nTryExec=subl\nExec=subl\nIcon=/opt/sublime_text/Icon/48x48/sublime-text.png\nTerminal=false\nStartupNotify=false" > ${HOME}/.config/autostart/subl.desktop

# transmission
echo -e "[Desktop Entry]\nType=Application\nEncoding=UTF-8\nName=Transmission GTK\nComment=Torrent client\nExec=transmission-gtk -m\nIcon=transmission-gtk\nIcon=/usr/share/icons/hicolor/48x48/apps/transmission.png\nTerminal=false" > ${HOME}/.config/autostart/transmission-gtk.desktop

# megasync x (does not show in systray, but it runs) ?
echo -e "[Desktop Entry]\nType=Application\nVersion=1.0\nGenericName=File Synchronizer\nName=MEGAsync\nComment=Easy automated syncing between your computers and your MEGA cloud drive.\nTryExec=megasync\nExec=megasync\nIcon=/usr/share/icons/hicolor/48x48/apps/mega.png\nTerminal=false\nCategories=Network;System;\nStartupNotify=false\n\nX-Desktop-File-Install-Version=0.23" > ${HOME}/.config/autostart/megasync.desktop

# remmina
echo -e "[Desktop Entry]\nVersion=1.0\nName=Remmina Applet\nComment=Connect to remote desktops through the applet menu\nIcon=org.remmina.Remmina\nExec=remmina -i\nTerminal=false\nType=Application\nHidden=true" > ${HOME}/.config/autostart/remmina-applet.desktop

# gnome-terminal (todo: let it be on top of other apps, but don't keep it on top)
echo -e "[Desktop Entry]\nType=Application\nEncoding=UTF-8\nName=Gnome Terminal\nComment=Terminal\nExec=gnome-terminal --geometry=1000x400\nIcon=/usr/share/help/C/gnome-terminal/figures/gnome-terminal-icon.png\nTerminal=true" > ${HOME}/.config/autostart/gnome-terminal.desktop

# caprine
# TODO: This does start the app, not no indicator shows up, therefore I cannot use it
# echo -e "[Desktop Entry]\nVersion=1.0\nName=Caprine\nComment=Facebook Messenger\nIcon=/opt/caprine/caprine.png\nExec=/opt/bin/caprine\nTerminal=false\nType=Application\nHidden=true" > ${HOME}/.config/autostart/caprine.desktop

# remove startup: gnome login sound
# also disable system sounds

# Remove apps I don't use
# TODO: rather don’t install packages if they should be removed afterwards
sudo dnf -y remove gnome-weather

# Configure Gnome 3 extensions
# TODO: update this
# sudo rm -rf /usr/share/gnome-shell/extensions && sudo ln -s ${XDG_GIT_DIR}/lnx_scripts/gnome/ext/global /usr/share/gnome-shell/extensions
# rm -rf ${HOME}/.local/share/gnome-shell/extensions && sudo ln -s ${XDG_GIT_DIR}/lnx_scripts/gnome/ext/local ${HOME}/.local/share/gnome-shell/extensions
# gsettings set org.gnome.shell enabled-extensions "['panel-date-format@keiii.github.com', 'drive-menu@gnome-shell-extensions.gcampax.github.com', 'openweather-extension@jenslody.de', 'extensions@abteil.org', 'status-area-horizontal-spacing@mathematical.coffee.gmail.com', 'noannoyance@sindex.com', 'disconnect-wifi@kgshank.net', 'hide-legacy-tray@shell-extensions.jonnylamb.com', 'nohotcorner@azuri.free.fr', 'advanced-settings-in-usermenu@nuware.ru', 'TopIcons@phocean.net', 'maximus-two@wilfinitlike.gmail.com', 'maximus-three@daman.4880.gmail.com', 'gnome-shell-extension-maximized-by-default@axe1.github.com', 'clipboard-indicator@tudmotu.com', 'remove-dropdown-arrows@mpdeimos.com', 'user-theme@gnome-shell-extensions.gcampax.github.com', 'activities-config@nls1729', 'gsconnect@andyholmes.github.io']"

# Disable suspension after laptop lid is closed
# Dependency: requires `gnome-tweaks` (or at least `gnome-tweak-tool-lid-inhibitor script)
# TODO: make this `gnome-tweaks`-independent
echo -e "[Desktop Entry]\nType=Application\nName=ignore-lid-switch-tweak\nExec=/usr/libexec/gnome-tweak-tool-lid-inhibitor" > ${HOME}/.config/autostart/ignore-lid-switch-tweak.desktop

# Set the dark theme
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Ubuntu Medium 11'
gsettings set org.gnome.desktop.interface font-name 'Ubuntu Medium 11'
gsettings set org.gnome.desktop.interface document-font-name 'Ubuntu 11'

# Disable touchpad (but keep the trackpoint enabled)
gsettings set org.gnome.desktop.peripherals.touchpad send-events 'disabled'

# Show week number in the panel calendar
gsettings set org.gnome.desktop.calendar show-weekdate 'true'

# Panel clock format extension configuration (needs [Panel Date Format](https://extensions.gnome.org/extension/1462/panel-date-format/))
# TODO: First, Install/restore the extension
dconf write /org/gnome/shell/extensions/panel-date-format/format "'(%-V)   %a %-d %b %Y, %l.%M%P %Z'"

# OpenWeather extension configuration (needs [OpenWeather](https://extensions.gnome.org/extension/750/openweather/))
# TODO: First, Install/restore the extension
dconf write /org/gnome/shell/extensions/openweather/appid-fc '059002f909d4ae7e475c74b808c0817e'  # Dark Sky API key
dconf write /org/gnome/shell/extensions/openweather/city '48.9734472,18.8026115>Kláštor pod Znievom, Alexandra Moyzesa, Kláštor pod Znievom, okres Martin, Žilinský kraj, Stredné Slovensko, 03843, Slovensko >1'
dconf write /org/gnome/shell/extensions/openweather/days-forecast 2
dconf write /org/gnome/shell/extensions/openweather/decimal-places 1
dconf write /org/gnome/shell/extensions/openweather/geolocation-provider 'openstreetmaps'
dconf write /org/gnome/shell/extensions/openweather/position-in-panel 'right'
dconf write /org/gnome/shell/extensions/openweather/pressure-unit 'hPa'
dconf write /org/gnome/shell/extensions/openweather/show-comment-in-forecast 'true'
dconf write /org/gnome/shell/extensions/openweather/show-text-in-panel 'true'
dconf write /org/gnome/shell/extensions/openweather/translate-condition 'true'
dconf write /org/gnome/shell/extensions/openweather/unit 'celsius'
dconf write /org/gnome/shell/extensions/openweather/use-default-owm-key 'true'
dconf write /org/gnome/shell/extensions/openweather/use-symbolic-icons 'true'
dconf write /org/gnome/shell/extensions/openweather/weather-provider 'darksky.net'
dconf write /org/gnome/shell/extensions/openweather/wind-direction 'true'
dconf write /org/gnome/shell/extensions/openweather/wind-speed-unit 'kph'

# Status area horizontal padding extension configuration
dconf write /org/gnome/shell/extensions/status-area-horizontal-spacing/hpadding 4

# Activities configuration extension configuration
# dconf write /org/gnome/shell/extensions/activities-config/activities-config-button-icon-path '/home/ts/.local/share/gnome-shell/extensions/activities-config@nls1729/face-smile-3.svg'
dconf write /org/gnome/shell/extensions/activities-config/activities-config-button-no-icon 'true'
dconf write /org/gnome/shell/extensions/activities-config/activities-config-button-no-text 'true'
dconf write /org/gnome/shell/extensions/activities-config/activities-config-button-removed 'true'
# dconf write /org/gnome/shell/extensions/activities-config/activities-config-button-text 'Activities'
dconf write /org/gnome/shell/extensions/activities-config/activities-config-hot-corner 'true'
# dconf write /org/gnome/shell/extensions/activities-config/activities-icon-padding 8
# dconf write /org/gnome/shell/extensions/activities-config/activities-text-padding 8
# dconf write /org/gnome/shell/extensions/activities-config/first-enable 'false'  # What is this for???
# dconf write /org/gnome/shell/extensions/activities-config/original-activities-button-text 'Activities'
dconf write /org/gnome/shell/extensions/activities-config/override-theme 'false'
dconf write /org/gnome/shell/extensions/activities-config/panel-background-color-hex-rgb '#000000'
dconf write /org/gnome/shell/extensions/activities-config/panel-hide-app-menu-button-icon 'true'
dconf write /org/gnome/shell/extensions/activities-config/panel-hide-rounded-corners 'true'
dconf write /org/gnome/shell/extensions/activities-config/panel-shadow-color-hex-rgb '#000000'
dconf write /org/gnome/shell/extensions/activities-config/pointer-barriers-supported 'true'
# dconf write /org/gnome/shell/extensions/activities-config/shell-theme-id 'null<|>user'

# Clipboard indicator extension configuration
dconf write /org/gnome/shell/extensions/clipboard-indicator/display-mode 0
dconf write /org/gnome/shell/extensions/clipboard-indicator/enable-keybindings 'false'

# Disconnect WiFi extension configuration
dconf write /org/gnome/shell/extensions/disconnect-wifi/show-reconnect-always 'true'

# Extensions extension configuration
dconf write /org/gnome/shell/extensions/extensions/position 'menu'
# dconf write /org/gnome/shell/extensions/extensions/show-add 'true'  # What is this for???

# GSConnect extension configuration
# dconf write /org/gnome/shell/extensions/gsconnect/device ''  # What is this for???
dconf write /org/gnome/shell/extensions/gsconnect/devices "['a7c3caab805f8a02']"  # a7c3caab805f8a02 = op3t
dconf write /org/gnome/shell/extensions/gsconnect/id '76ebcbf4-f765-4b3d-b093-83e313ebbad0'  # What is this for???
dconf write /org/gnome/shell/extensions/gsconnect/messaging/window-maximized 'true'
# dconf write /org/gnome/shell/extensions/gsconnect/name 'e531.domcek.lan'  # TODO: set the FQDN
dconf write /org/gnome/shell/extensions/gsconnect/preferences/window-maximized 'true'
dconf write /org/gnome/shell/extensions/gsconnect/show-indicators 'true'

# TopIcons Git extension configuration
dconf write /org/gnome/shell/extensions/topicons/icon-opacity 255
dconf write /org/gnome/shell/extensions/topicons/icon-spacing 4
dconf write /org/gnome/shell/extensions/topicons/tray-pos 'right'

# org.gnome.online-accounts
# org.gnome.Contacts
# org.gnome.desktop.datetime
# org.gnome.desktop.default-applications
# org.gnome.desktop.default-applications.office
# org.gnome.desktop.default-applications.office.calendar
# org.gnome.desktop.default-applications.office.tasks
# org.gnome.desktop.default-applications.terminal
# org.gnome.desktop.interface
# org.gnome.desktop.lockdown
# org.gnome.desktop.media-handling
# org.gnome.desktop.notifications
# org.gnome.desktop.notifications.application:/
# org.gnome.GWeather
# org.gnome.Weather.Application
# org.gnome.packagekit
# org.gnome.seahorse
# org.gnome.seahorse.manager
# org.gnome.seahorse.window:/
# org.gnome.settings-daemon.plugins.account
# org.gnome.settings-daemon.plugins.color
# org.gnome.settings-daemon.plugins.housekeeping
# org.gnome.settings-daemon.plugins.media-keys
# org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/
# org.gnome.settings-daemon.plugins.xsettings
# org.gnome.shell
# org.gnome.shell.keybindings
# org.gnome.shell.keyboard
# org.gnome.shell.overrides
# org.gnome.eog[.*]  # set the background
# org.gnome.software