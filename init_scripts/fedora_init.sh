#!/bin/bash

# This function sets up a clean-installed vanilla Fedora 30 Gnome 3 to my taste

# author:  Tukusej's Sirs
# date:    27 October 2019
# version: 0.5


# User-dependent variables
temp="/dev/shm"
os_probe="no"
model=$(sudo dmidecode -t system | grep -Po "Version: [ThinkPad Edge]* \K.*$" | tr '[:upper:]' '[:lower:]')
host_name="${USER}.${model}.lan"

# Set up hostname
hostnamectl set-hostname $host_name
echo $host_name | sudo tee /etc/hostname
HOSTNAME="$host_name"

# Set swappiness to 10 %
sudo sysctl vm.swappiness=10

# Add repos
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg  # For Sublime Text 3
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo  # For Sublime Text 3

# Set maximum installed versions of any package to two
# This also automatically removes all other kernels
sudo su -c "sed -i 's/installonly_limit=[0-9]\+/installonly_limit=2/' /etc/dnf/dnf.conf"

# Update dnf cache and packages
sudo dnf -y update
sudo dnf -y upgrade

# Install additional packages
sudo dnf -y install android-tools audacious audacity autoconf automake bash-completion bison bzip2 cabextract cifs-utils cmake coreutils dkms ffmpeg firefox folks gcc gcc-c++ gimp git git-lfs gnome-tweak-tool gourmet gparted html2text jq kernel-devel kernel-headers libffi-devel libreoffice libreoffice-langpack-{cs,hu,he,el,sk,en} libtool libyaml-devel make ntfs-3g ntfsprogs meson openssh* openssl-devel p7zip patch perl perl-core perl-CPAN pv pygobject2 python-requests readline readline-devel ruby-devel sqlite-devel sublime-text tesseract tesseract-devel tesseract-langpack-{ces,deu,heb,hun,lat,slk} testdisk unrar vlc xclip yaru-theme zlib zlib-devel smartmontools bc python2-gitlab colordiff unace  # iconv-devel

# gnome tilix (terminal)
# gmome seahorse (keyring mgmt)
# gnome recipes (try out)
# gnome pdfmod (check out)
# gnome maps
# gnome keysign (gpg keys / qr codes scanner)
# gnome gucharmap
# gnome Grappe
# gnome documents
# gnome Déjà Dup Backup Tool
# ??? gnome-commander
# gnome break timer
# gnome books
# gnome calendar (not california)
# gnome ocrfeeder (add sk translation)
# gnome smuxi OR gnome empathy (chat)
# gnome-subtitles
# gnome easytag
# gnome-soundrecorder

# ntfs-3g        # For ntfs file system management
# android-tools  # adb and fastboot
# gparted        # Partition and disk management
# git            # Git
# gcc-c++ patch readline readline-devel zlib zlib-devel libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison iconv-devel sqlite-devel  # Ruby/RVM dependencies
# xclip          # For clipboard management

# Clone `lnx_scripts` repos
mkdir -p ${temp}/lnx_scripts
git clone https://gitlab.com/tukusejssirs/lnx_scripts.git ${temp}/lnx_scripts

# Change XDG_GIT_DIR variable
XDG_GIT_DIR=$(grep -Po "XDG_GIT_DIR=\"\K[^\"]*" ${temp}/lnx_scripts/user-dirs/user-dirs.dirs_$USER | sed "s#\$HOME#$HOME#" -)
mkdir -p "$XDG_GIT_DIR"
mv "$temp/lnx_scripts" "$XDG_GIT_DIR"

# Change user folder names
if [[ $(uname -r | grep -o "Microsoft$") != "Microsoft" ]]; then
	if [[ ${HOME}/.config/user-dirs.dirs ]]; then mv ${HOME}/.config/user-dirs.dirs{,.bak}; fi
	if [[ ${HOME}/.config/user-dirs.locale ]]; then mv ${HOME}/.config/user-dirs.locale{,.bak}; fi
	for n in $(cat ${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.dirs_${USER} | grep -oP "^XDG[^=]*=\"\K[^\"]*" | tr '\n' ' ' | sed "s#\$HOME#$HOME#g" -); do mkdir -p "$n"; done
	ln -s ${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.dirs_${USER} ${HOME}/.config/user-dirs.dirs
	ln -s ${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.locale_${USER} ${HOME}/.config/user-dirs.locale
fi
source ~/.bashrc

# Restore `ssh` known hosts list
mkdir -p ${HOME}/.ssh
rm -rf ${HOME}/.ssh/known_hosts
cp ${XDG_GIT_DIR}/lnx_data/ssh/known_hosts ${HOME}/.ssh/known_hosts
chmod 600 ${HOME}/.ssh/known_hosts

# Run `git_init.sh`
${XDG_GIT_DIR}/lnx_scripts/init_scripts/git_init.sh

# Change `lnx_scripts` remote url (https to git protocol transition)
cd ${XDG_GIT_DIR}/lnx_scripts
git remote rm origin
git remote add origin "git@gitlab.com:tukusejssirs/lnx_scripts.git" &>/dev/null
git branch --set-upstream-to=origin/master master &>/dev/null
git push --set-upstream origin master &>/dev/null
git remote set-head origin -a &>/dev/null

# Run `bash_init.sh`
test_bash=$(whereis bash | grep -Po "bash: \K.*$")
if [ "$test_bash" != "" ]; then
	${XDG_GIT_DIR}/lnx_scripts/init_scripts/bash_init.sh "$XDG_GIT_DIR"
fi

# Run `fish_init.sh`
test_fish=$(whereis fish | grep -Po "fish: \K.*$")
if [ "$test_fish" != "" ]; then
	fish ${XDG_GIT_DIR}/lnx_scripts/init_scripts/fish_init.fish
fi

# Clone some repos
mkdir -p ${XDG_GIT_DIR}/{others/dotfiles,ofcl/rodokmen,lnx_data}
git clone git@notabug.org:demure/dotfiles.git ${XDG_GIT_DIR}/others/dotfiles
git clone git@gitlab.com:ofcl/rodokmen.git ${XDG_GIT_DIR}/ofcl/rodokmen
git clone git@gitlab.com:tukusejssirs/lnx_data.git ${XDG_GIT_DIR}/lnx_data

# Source the new ~/.bashrc
source ${HOME}/.bashrc

# If Gnome is installed, set some Gnome-specific settings
gstngs_test=$(whereis gsettings | grep -Po "gsettings: \K.*$")
if [ "$gstngs_test" != "" ]; then
	${XDG_GIT_DIR}/lnx_scripts/init_scripts/gnome_init.sh
fi

# Install programs using pre-made bash scripts
${XDG_GIT_DIR}/lnx_scripts/bash_progs/rvm.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/chrome-gnome-shell.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/flash_player.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/virtualbox.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/git_src.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/automake_src.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/nano_src.sh  # Requires `git_src` and `automake_src` to be installed in advance
${XDG_GIT_DIR}/lnx_scripts/bash_progs/gollum.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/mailspring.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/bash_src.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/libtool_src.sh
# ${XDG_GIT_DIR}/lnx_scripts/bash_progs/sane-backends_src.sh  # Requires `automake_src` and `libtool_src` to be installed in advance; this does not work yet
${XDG_GIT_DIR}/lnx_scripts/bash_progs/imagemagick_src.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/fonts.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/teamviewer.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/texlive.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/ms-sys.sh  # error (no makefile found)
sudo ${XDG_GIT_DIR}/lnx_scripts/bash_progs/wine.sh  # not working yet
${XDG_GIT_DIR}/lnx_scripts/bash_progs/remmina.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/mscore.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/winbox.sh

if [ "$os_probe" = "yes" ]; then
	# Add Windows 10 grub entry
	#sudo sh -c 'echo -e "menuentry "Windows 10" {\n    set root=(hd1,gpt1)\n    chainloader /EFI/Microsoft/Boot/bootmgfw.efi\n}" > /etc/grub.d/40_custom'
	#sudo grub2-mkconfig --output=/boot/grub2/grub.cfg
	sudo os-prober
fi

# Auto-mount win10 partition
if [ -e /dev/disk/by-uuid/A832A18632A159D8 ]; then
	# Auto-mount win10 partition
	mkdir ${HOME}/win10
	sudo sh -c 'echo -e "UUID="A832A18632A159D8" /home/ts/win10          ntfs    defaults        0 0" >> /etc/fstab'
else
	echo -e "\nWARNING: I could not find A832A18632A159D8 drive to mount win10.\n"
fi

# Install WiFi driver
# src: https://tanmaync.wordpress.com/2017/12/02/install-broadcom-bcm43142-wifi-drivers-fedora/
# check repo links here: https://rpmfusion.org/Configuration/
# TODO: test if the driver is already installed: `ip a | sed 's/^[0-9]*: //g' - | grep -o "^w[^:]*"`
# test_wifi=$(lspci | grep -o BCM43142)
# if [ "$test_wifi" = "BCM43142" ]; then
# 	sudo yum -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %centos).noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %centos).noarch.rpm
# 	sudo yum -y install kmod-wl
# fi

# Enable root login in GDM
sudo sh -c "sed -i 's/auth required pam_succeed_if.so user != root quiet/# &/' /etc/pam.d/gdm"

# Enable root login in SSH
sudo sed -i 's/#PermitRootLogin.*/PermitRootLogin yes/' /etc/ssh/sshd_config

# Change `root` password
sudo passwd root

# Restore `.mozilla`
rm -rf ${HOME}/.mozilla
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/mozilla_${USER} ${HOME}/.mozilla

# Restore Sublime Text 3
rm -rf ${HOME}/.config/sublime-text-3
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/subl_${USER} ${HOME}/.config/sublime-text-3

# Restore Shrew (`.ike`)
rm -rf ${HOME}/.ike
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/ike_ts ${HOME}/.ike

# Restore Remmina
rm -rf ${HOME}/.remmina ${HOME}/.local/share/remmina
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/remmina_ts ${HOME}/.remmina
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/remmina_ts ${HOME}/.local/share/remmina

# Restore Mailspring
rm -rf ${HOME}/.config/Mailspring
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/mailspring_ts ${HOME}/.config/Mailspring

# Restore Audacious
rm -rf ${HOME}/.config/audacious
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/audacious_${USER} ${HOME}/.config/audacious

# Restore GSConnect
rm -rf ${HOME}/.config/gsconnect
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/gsconnect ${HOME}/.config/gsconnect

# Restore TeamViewer
rm -rf ${HOME}/.config/teamviewer
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/teamviewer_ts ${HOME}/.config/teamviewer

# Restore Gourmet
rm -rf ${HOME}/.gourmet
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/gourmet ${HOME}/.gourmet

# Setup `/etc/hosts`
mkdir -p ${XDG_GIT_DIR}/others/stevenblack_hosts
git clone git@github.com:StevenBlack/hosts.git ${XDG_GIT_DIR}/others/stevenblack_hosts
cat ${XDG_GIT_DIR}/lnx_data/hosts/hosts_local_domains ${XDG_GIT_DIR}/others/stevenblack_hosts/alternates/fakenews-gambling-porn/hosts | sudo tee /etc/hosts &>/dev/null