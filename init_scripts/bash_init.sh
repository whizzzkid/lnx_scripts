#!/bin/bash

# Bash files initialisation script

# author:  Tukusej's Sirs
# date:    20 January 2020
# version: 1.9


if [ -f "${HOME}/.config/user-dirs.dirs" ]; then
	source "${HOME}/.config/user-dirs.dirs"
elif [ -f "${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.dirs_${USER}" ]; then
	source "${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.dirs_${USER}"
elif [ -f "${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.dirs_ts" ]
	echo "WARNING: There is not user-dirs.dirs file for $USER user. Using one for ts user." 1>&2
	source "${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.dirs_ts"
fi

if [ ! "${XDG_GIT_DIR}" ]; then
	echo "WARNING: XDG_GIT_DIR is not defined. Using /git." 1>&2
	XDG_GIT_DIR='/git'
fi

if [[ "${HOME}/.bash_profile" ]]; then mv "${HOME}/.bash_profile" "${HOME}/.bash_profile.bak"; fi
if [[ "${HOME}/.bashrc" ]]; then mv "${HOME}/.bashrc" "${HOME}/.bashrc.bak"; fi
if [[ "${HOME}/.bash_logout" ]]; then mv "${HOME}/.bash_logout" "${HOME}/.bash_logout.bak"; fi

ln -s "${XDG_GIT_DIR}/lnx_scripts/bashrc/.bashrc" "${HOME}/.bashrc"
ln -s "${HOME}/.bashrc" "${HOME}/.bash_profile"
ln -s "${XDG_GIT_DIR}/lnx_scripts/bash_logout/.bash_logout" "${HOME}/.bash_logout"

source "${HOME}/.bashrc"


# rm -rf ~/.b*.bak