#!/bin/bash

# This function sets up a clean-installed vanilla Centos 7 Gnome 3 to my taste

# author:  Tukusej's Sirs
# date:    27 October 2019
# version: 0.5

# User-dependent variables
temp="/dev/shm"
os_probe="no"

install_rvm="yes"
install_gollum="yes"
install_chrome_gnome_shell="yes"
install_flash="yes"
install_remmina="yes"
install_nano="yes"
install_git_src="yes"
install_automake_src="yes"
install_mailspring="yes"
install_bash_src="yes"
install_sane_src="yes"
install_libtool_src="yes"
install_imagemagick_src="yes"
install_fonts="yes"
install_teamviewer="yes"
# install_texlive="yes"

# If gnome is installed, set some gnome-specific settings
gstngs_test=$(whereis gsettings | grep -Po "gsettings: \K.*$")
if [ "$gstngs_test" != "" ]; then
	${XDG_GIT_DIR}/lnx_scripts/init_scripts/gnome_init.sh
fi

# Set swappiness to 10 %
sudo sysctl vm.swappiness=10

# Update and upgrade installed packages

# Extra repos
# sudo install -y epel-release
# sudo yum localinstall --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm
# sudo yum localinstall --nogpgcheck https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm

# sudo rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
# sudo rpm -Uvh https://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm

# sudo rpm -Uvh http://mirror.ghettoforge.org/distributions/gf/gf-release-latest.gf.el7.noarch.rpm

# sudo rpm --import http://wiki.psychotic.ninja/RPM-GPG-KEY-psychotic
# sudo rpm -ivh http://packages.psychotic.ninja/6/base/i386/RPMS/psychotic-release-1.0.0-1.el6.psychotic.noarch.rpm


# Set maximum kernel versions to two
# This automatically removes all other kernels
sed -i 's/installonly_limit=[0-9]\+/installonly_limit=2/' /etc/yum.conf

sudo yum -y install epel-release
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg  # For Sublime Text 3
sudo yum-config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo  # For Sublime Text 3
sudo yum -y update
sudo yum -y upgrade

# Install additional packages
sudo yum -y install ntfs-3g android-tools gparted git git-lfs gcc-c++ patch readline readline-devel zlib zlib-devel libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison iconv-devel sqlite-devel xclip ruby-devel cmake coreutils meson pygobject2 python-requests jq libreoffice libreoffice-langpack-{cs,hu,he,el,sk,en} p7zip vlc sublime-text pv perl gcc dkms kernel-devel kernel-headers gimp ImageMagick ffmpeg tesseract tesseract-devel tesseract-langpack-{ces,des,des_frak,heb,hun,lat,slk,slk_frak} ntfsprogs cabextract cifs-utils perl-core ubuntu-font-family unrar wine-systemd winetools bash-completion perl-CPAN openssh* gnome-tweak-tool firefox audacious audacity gourmet testdisk folks smartmontools bc python2-gitlab colordiff unace
# geary
# gnome-weather
# gnome-tweek-tool / tweaks
# gnome tilix (terminal)
# gmome seahorse (keyring mgmt)
# gnome recipes (try out)
# gnome pdfmod (check out)
# gnome maps
# gnome keysign (gpg keys / qr codes scanner)
# gnome gucharmap
# gnome Grappe
# gnome documents
# gnome Déjà Dup Backup Tool
# ??? gnome-commander
# gnome break timer
# gnome books
# gnome calendar (not california)
# gnome ocrfeeder (add sk translation)
# gnome smuxi OR gnome empathy (chat)
# gnome-subtitles
# gnome easytag
# gnome-soundrecorder

# ntfs-3g        # For ntfs file system management
# android-tools  # adb and fastboot
# gparted        # Partition and disk management
# git            # Git
# gcc-c++ patch readline readline-devel zlib zlib-devel libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison iconv-devel sqlite-devel  # Ruby/RVM dependencies
# xclip          # For clipboard management

# Install RVM (ruby manager)
if [ "$install_rvm" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/rvm.sh
fi

if [ "$os_probe" = "yes" ]; then
	# Add Windows 10 grub entry
	#sudo sh -c 'echo -e "menuentry "Windows 10" {\n    set root=(hd1,gpt1)\n    chainloader /EFI/Microsoft/Boot/bootmgfw.efi\n}" > /etc/grub.d/40_custom'
	#sudo grub2-mkconfig --output=/boot/grub2/grub.cfg
	sudo os-prober
fi

# Auto-mount win10 partition
if [ -e /dev/disk/by-uuid/A832A18632A159D8 ]; then
	# Auto-mount win10 partition
	mkdir ${HOME}/win10
	sudo sh -c 'echo -e "UUID="A832A18632A159D8" /home/ts/win10          ntfs    defaults        0 0" >> /etc/fstab'
else
	echo -e "\nWARNING: I could not find A832A18632A159D8 drive to mount win10.\n"
fi

# Install WiFi driver
# src: https://tanmaync.wordpress.com/2017/12/02/install-broadcom-bcm43142-wifi-drivers-fedora/
# check repo links here: https://rpmfusion.org/Configuration/
# TODO: test if the driver is already installed: `ip a | sed 's/^[0-9]*: //g' - | grep -o "^w[^:]*"`
test_wifi=$(lspci | grep -o BCM43142)
if [ "$test_wifi" = "BCM43142" ]; then
	sudo yum -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %centos).noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %centos).noarch.rpm
	sudo yum -y install kmod-wl
fi

# Install Gollum
if [ "$install_gollum" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/gollum.sh
fi

# Install chrome-gnome-shell
if [ "$install_chrome_gnome_shell" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/chrome-gnome-shell.sh
fi

# Install Adobe Flash Player
if [ "$install_flash" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/flash_player.sh
fi

# Install virtualbox
${XDG_GIT_DIR}/lnx_scripts/bash_progs/virtualbox.sh

#  Install git from source
if [ "$install_git_src" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/git_src.sh
fi

# Intall automake from source
if [ "$install_automake_src" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/automake_src.sh
fi

# Install nano from source
if [ "$install_nano" = "yes" ] && [ "$install_git_src" = "yes" ] && [ "$install_automake_src" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/nano_src.sh
fi

# Install mailspring
if [ "$install_mailspring" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/mailspring.sh
fi

# Install latest bash
if [ "$install_bash_src" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/bash_src.sh
fi

# Install latest libtool
if [ "$install_libtool_src" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/libtool_src.sh
fi

# Install latest sane-backends
# $nw
# if [ "$install_sane_src" = "yes" ] && [ "$install_automake_src" = "yes" ] && [ "$install_libtool_src" = "yes" ]; then
# 	${XDG_GIT_DIR}/lnx_scripts/bash_progs/sane-backends_src.sh  # this does not work yet
# fi

# Install imagemagick from source
if [ "$install_imagemagick_src" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/imagemagick_src.sh
fi

# Install fonts (Ubuntu, Linux Libertine G and Libertinus)
if [ "$install_fonts" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/fonts.sh
fi

# Install teamviewer
if [ "$install_teamviewer" = "yes" ]; then
	${XDG_GIT_DIR}/lnx_scripts/bash_progs/teamviewer.sh
fi

# Install TexLive
${XDG_GIT_DIR}/lnx_scripts/bash_progs/texlive.sh

# Install Remmina
${XDG_GIT_DIR}/lnx_scripts/bash_progs/remmina.sh

# Download Mikrotik WinBox
${XDG_GIT_DIR}/lnx_scripts/bash_progs/winbox.sh