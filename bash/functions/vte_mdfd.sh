# Copyright © 2006 Shaun McCance <shaunm@gnome.org>
# Copyright © 2013 Peter De Wachter <pdewacht@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# 28 Sep 2019: Tukusej’s Sirs modified `/etc/profile.d/vte.sh` script by stripping down all unnecessary parts for his usage
# (src: https://unix.stackexchange.com/questions/93476/gnome-terminal-keep-track-of-directory-in-new-tab#comment219157_93477)

# Not an interactive shell?
[[ $- == *i* ]] || return 0

# Not running under vte?
[ "${VTE_VERSION:-0}" -ge 3405 ] || return 0

__vte_urlencode() (
	# This is important to make sure string manipulation is handled
	# byte-by-byte.
	LC_ALL=C
	str="$1"
	while [ -n "$str" ]; do
		safe="${str%%[!a-zA-Z0-9/:_\.\-\!\'\(\)~]*}"
		printf "%s" "$safe"
		str="${str#"$safe"}"
		if [ -n "$str" ]; then
			printf "%%%02X" "'$str"
			str="${str#?}"
		fi
	done
)

__vte_prompt_command() {
	local command=$(HISTTIMEFORMAT= history 1 | sed 's/^ *[0-9]\+ *//')
	command="${command//;/ }"
	local pwd='~'
	printf "\033]7;file://%s%s\007" "${HOSTNAME:-}" "$(__vte_urlencode "${PWD}")"
}

case "$TERM" in
	xterm*|vte*)
		[ -n "$BASH_VERSION" ] && PROMPT_COMMAND="${PROMPT_COMMAND};__vte_prompt_command"
		;;
esac