#!/bin/bash

# Prompt Settings of Tukusej's Sirs

# author:  Tukusej's Sirs
# date:    7 January 2020
# version: 1.11

# Based upon multiple online sources, especially on:
# - demure's bashrc sub source prompt script (https://notabug.org/demure/dotfiles/raw/master/subbash/prompt)
# - stripped down `/etc/profile.d/vte.sh` (inspiration: https://unix.stackexchange.com/questions/93476/gnome-terminal-keep-track-of-directory-in-new-tab#comment219157_93477)

# This changes the PS1
# - the `history -a` part is to add the run command to bash history immediately, not only after the shell quits
# - the `term_title_prompt` sets the terminal title
# - the `prompt` sets the prompt
# - the `__vte_prompt_command` sets the CWD path to the previously open folder (defaults to user home directory)
export PROMPT_COMMAND="history -a; term_title_prompt; prompt; __vte_prompt_command"

function term_title_prompt(){
	# This condition is based on this answer and its comments: https://serverfault.com/a/187719/514866
	if [ "$(who am i | grep -Po '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')" ]; then
		# Remote host
		printf "\033]0;%s@%s\007" "${USER}" "${HOSTNAME%%.*}"
	else
		# Local host
		if [ $(echo "$PWD" | grep -o "$XDG_GIT_DIR/itens/controlserver/") ]; then
			printf "\033]0;%s\007" "${PWD/#$XDG_GIT_DIR\/itens\/controlserver\//}"
		elif [ $(echo "$PWD" | grep -o "$XDG_GIT_DIR/itens/") ]; then
			printf "\033]0;%s\007" "${PWD/#$XDG_GIT_DIR\/itens\//}"
		elif [ $(echo "$PWD" | grep -o "$XDG_GIT_DIR/") ]; then
			printf "\033]0;%s\007" "${PWD/#$XDG_GIT_DIR\//}"
		else
			TILDE='~'
			printf "\033]0;%s\007" "${PWD/#$HOME/$TILDE}"
		fi
	fi
}

__vte_urlencode() (
	# This is important to make sure string manipulation is handled
	# byte-by-byte.
	LC_ALL=C
	str="$1"
	while [ -n "$str" ]; do
		safe="${str%%[!a-zA-Z0-9/:_\.\-\!\'\(\)~]*}"
		printf "%s" "$safe"
		str="${str#"$safe"}"
		if [ -n "$str" ]; then
			printf "%%%02X" "'$str"
			str="${str#?}"
		fi
	done
)

__vte_prompt_command() {
	local command=$(HISTTIMEFORMAT= history 1 | sed 's/^ *[0-9]\+ *//')
	command="${command//;/ }"
	local cwd='~'
	printf "\033]7;file://%s%s\007" "${HOSTNAME:-}" "$(__vte_urlencode "${cwd}")"
}

function prompt(){
	local EXIT="$?"  # This needs to be first
	PS1=""
	local userHost=""

	# Newline
	# Adds a newline after the previous command output (and before the PS1)
	PS1+="\n"

	## Date
	PS1+="${fdefault}[${magenta}\D{%l.%M%P}${fdefault}]"

	# Exit code
	if [ $EXIT != 0 ]; then
		PS1+="${red}${EXIT}${fdefault}"
	else
		PS1+="${fdefault}${EXIT}"
	fi

	### Host test
	# This condition is based on this answer and its comments: https://serverfault.com/a/187719/514866
	if [ "$(who am i | grep -Po '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')" ]; then
		# Remote host
		local userHost="${lyellow}\u${red}@${lyellow}\h:${fdefault}"
	else
		# Local host
		local userHost="${lyellow}\u@\h:${fdefault}"
	fi

	PS1+=" ${userHost}"

	# Check background and stopped jobs
	# Background running jobs
	local BKGJBS=$(jobs -r | wc -l | tr -d ' ')
	if [ ${BKGJBS} -gt 0 ]; then
		PS1+="${yellow}[bg:${BKGJBS}]${fdefault}"
	fi

	## Stopped jobs
	local STPJBS=$(jobs -s | wc -l | tr -d ' ')
	if [ ${STPJBS} -gt 0 ]; then
		PS1+=" ${yellow}[stp:${STPJBS}]${fdefault}"
	fi

	# Git status
	local GStatus="$(git status --porcelain=2 -b 2>/dev/null | tr '\n' ':')"
	local gss="$(git status --porcelain=1 2>/dev/null)"

	if [ -n "${GStatus}" ]; then
		### Fetch Time Check ### {{{
		local LAST=$(stat -c %Y $(git rev-parse --git-dir 2>/dev/null)/FETCH_HEAD 2>/dev/null)
		if [ -n "${LAST}" ]; then
			local TIME=$(echo $(($(date +"%s") - ${LAST})))
			## Check if more than 60 minutes since last run of `${GStatus}`
			if [ "${TIME}" -gt "3600" ]; then
				git fetch 2>/dev/null
				PS1+=' +'
				## Refresh var
				local GStatus="$(git status --porcelain=2 -b 2>/dev/null | tr '\n' ':')"
				local gss="$(git status --porcelain=1 2>/dev/null)"
			fi
		fi
		### End Fetch Check ### }}}

		### Test For Changes ### {{{
		local GChanges="$(echo -n "$gss" | wc -m)"
		if [ "${GChanges}" != "0" ]; then
			local GitColor=${lred}
		fi
		### End Test Changes ### }}}

		### Find Branch ### {{{
		local GBranch="$(echo ${GStatus} | awk 'match($0,/# branch.head [^ :]+/) {print substr($0,RSTART+14,RLENGTH-14)}')"
		if [ -n "${GBranch}" ]; then
			GBranch="[${GBranch}]"			  ## Add brackets for final output. Will now test against brackets as well.
			if [ "${GBranch}" == "[master]" ]; then
				local GBranch="[M]"			 ## Because why waste space
			fi
			## Test if in detached head state, and set output to first 8char of hash
			if [ "${GBranch}" == "[(detached)]" ]; then
				GBranch="($(echo ${GStatus} | awk 'match($0,/branch.oid [0-9a-fA-F]+/) {print substr($0,RSTART+11,RLENGTH-11)}' | cut -c1-8))"
			fi
		  else
			## Note: No braces applied to emphasis that there is an issue, and that you aren't in a branch named "ERROR".
			local GBranch="ERROR"			   ## It could happen?
		fi
		### End Branch ### }}}

		PS1+=" ${GitColor}${GBranch}${fdefault}"	## Add result to prompt

		### Find Commit Status ### {{{

		## Add 0 to knock off the '+'
		local GAhead="$(echo ${GStatus} | awk 'match($0,/# branch.ab \+[0-9]+ \-[0-9]+/) {split(substr($0,RSTART+12,RLENGTH-12),s," "); V=s[1]+0} END {if(V>0){print V}}')"
		if [ -n "${GAhead}" ]; then
			PS1+="${lblue}↑${fdefault}${GAhead}"	  ## Ahead
		fi

		## Needs a `git fetch`
		## Multiply by -1 to remove the '-'
		local GBehind="$(echo ${GStatus} | awk 'match($0,/# branch.ab \+[0-9]+ \-[0-9]+/) {split(substr($0,RSTART+12,RLENGTH-12),s," "); V=s[2]*-1} END {if(V>0){print V}}')"
		if [ -n "${GBehind}" ]; then
			PS1+="${lred}↓${fdefault}${GBehind}"	 ## Behind
		fi

		local current_branch_changes=$(echo -n "$gss" | wc -l)
		local file_modified_staged_remodified=$(grep '^MM' <<< "$gss" | wc -l)
		local file_modified_nonstaged=$(grep '^ M' <<< "$gss" | wc -l)
		local file_modified_staged=$(grep '^M ' <<< "$gss" | wc -l)
		local file_added=$(grep '^A ' <<< "$gss" | wc -l)
		local file_added_remodified=$(grep '^AM' <<< "$gss" | wc -l)
		local file_deleted=$(grep '^D' <<< "$gss" | wc -l)
		local file_untracked=$(grep '^??' <<< "$gss" | wc -l)

		# Output a space between branch name and list of changes (only when there are some changes)
		if [ "${current_branch_changes}" -gt "0" ]; then
			PS1+=" "
		fi

		if [ "${file_modified_staged_remodified}" -gt "0" ]; then
			PS1+="${magenta}MM${fdefault}${file_modified_staged_remodified}"
		fi

		if [ "${file_modified_nonstaged}" -gt "0" ]; then
			PS1+="${lred}M-${fdefault}${file_modified_nonstaged}"
		fi

		if [ "${file_modified_staged}" -gt "0" ]; then
			PS1+="${lblue}M+${fdefault}${file_modified_staged}"
		fi

		if [ "${file_added}" -gt "0" ]; then
			PS1+="${yellow}A${fdefault}${file_added}"
		fi

		if [ "${file_added_remodified}" -gt "0" ]; then
			PS1+="${lyellow}AM${fdefault}${file_added_remodified}"
		fi

		if [ "${file_deleted}" -gt "0" ]; then
			PS1+="${red}D${fdefault}${file_deleted}"
		fi

		if [ "${file_untracked}" -gt "0" ]; then
			PS1+="${cyan}?${fdefault}${file_untracked}"
		fi

		### End Commit Status ### }}}
	fi

	# Current working Directory
	TILDE='~'
	local cwd="${PWD/#$HOME/$TILDE}"

	PS1+=" ${cwd}\n\$ "  # Currect working directory, newline and \$
}