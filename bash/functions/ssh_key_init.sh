#!/bin/bash

# This function creates a new SSH key for current local user and sets it up on the remote server

# author:  Tukusej's Sirs
# date:    7 October 2019
# version: 1.1

# Usage: ssh_key_init user@host:port [name]

function ssh_key_init(){
	if [ ! $1 ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
		echo -e "ERROR: No arguments supplied.\n\nUsage: ssh_key_init user@host:port [name]"
		return 1
	fi
	user=$(echo "$1" | grep -o "^[^@]*")
	host=$(echo "$1" | grep -oP "@\K[^:]*")
	port=$(echo "$1" | grep -oP ":\K.*")
	if [ "$2" = "" ]; then
		name="${user}_${host}"
		id_key="${user}@${host}"
	else
		name="$2"
		id_key="$2"
	fi

	ssh-keygen -t ed25519 -a 100 -f "${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}" -P ""
	chmod 600 "${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}"
	ssh-add "${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}"

	if [ "$port" = "" ]; then
		echo -e "\nHost ${name}\n    Hostname      ${host}\n    IdentityFile  ${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}\n    User          ${user}" >> ${HOME}/.ssh/config
		ssh-copy-id $user@$host
	else
		echo -e "\nHost ${name}\n    Hostname      ${host}\n    IdentityFile  ${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}\n    Port          ${port}\n    User          ${user}" >> ${HOME}/.ssh/config
		ssh-copy-id -p $port $user@$host
	fi
	echo -e "${lmagenta}For ${user}@${host}:${port} an SSH key was created and set up.${fdefault}"
}