#!/bin/bash

# This program sets brightness up or down by $step changing the int value in $brightness file
#
# author:  Tukusej's Sirs
# date:    11 June 2019
# version: 1.1

# $1       [ up | down ] (required)
# $2       [ step ]      (default: 500)

# return codes:
# 0        success
# 1        no action input
# 2        no valid action input
# 3        return on user input (user did not have write access to $brightnes)

function brightness(){
	# Variables
	local brightness="/sys/class/backlight/intel_backlight/brightness"
	local max=$(cat /sys/class/backlight/intel_backlight/max_brightness)

	# if [ "$1" ] && [ "$1" = "[uU][pP]*|[dD]|[dD][oO][wW][nN]" ]; then
		# echo -n
	# elif [ "$1" ]; then

	if [ "$1" ] && [[ "$1" != [uU][pP]* ]] && [[ "$1" != [dDuU]* ]] && [[ "$1" != [dD][oO][wW][nN] ]]; then
		echo "ERROR: Action $1 is not valid."
		return 2
	elif [ "$1" = "" ]; then
		echo "ERROR: No action was input."
		return 1
	fi

	if [ $2 ] && [ $2 -gt 0 ] && [ $2 -le $max ]; then
		local step=$2
	else
		local step=500
	fi

	if [ ! -w $brightness ]; then
		echo "WARNING: This scripts needs write permission to $brightness file."
		echo
		echo "You have two options:"
		echo "   [1] add write access to the file (you will be prompted for one-time sudo password);"
		echo "   [2] run the sudo command when necessery (you will be prompted for sudo password each time you run this script);"
		echo "   [3] quit the script."
		echo
		echo "What do you want to do? (default: [1])"
		while [ "$ans" != "1" ] && [ "$ans" != "2" ] && [ "$ans" != "3" ]; do
			local ans
			read ans
			case $ans in
				1|'') sudo chmod o+w $brightness ;;
				2)    su_user="sudo"             ;;
				3)    return 3        ;;
				*)    echo "$ans is not valid. Please enter valid option."
			esac
		done
	fi

	case $1 in
		[uU][pP]*)
			if [[ $(( $(cat $brightness) + $step )) -lt $max ]]; then
				echo $(( $(cat $brightness) + $step )) | $su_user tee $brightness
			else
				echo $max > $brightness
			fi
		;;
		[dD]|[dD][oO][wW][nN])
			if [[ $(( $(cat $brightness) - $step )) -le 0 ]]; then
				echo 0 > $brightness
			else
				echo $(( $(cat $brightness) - $step )) | $su_user tee $brightness
			fi
		;;
		*)
			echo "ERROR: Action $action is not valid."
			return 2
		;;
	esac

	return 0
}