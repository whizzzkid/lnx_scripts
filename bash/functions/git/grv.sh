# List remotes with URLs with their references

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function grv(){
	git remote -v $@
}