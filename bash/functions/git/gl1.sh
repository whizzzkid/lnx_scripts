# Show git log with full commit hashes and messages

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gl1(){
	git log --pretty=oneline
}