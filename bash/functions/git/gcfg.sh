# Used to set or get git config

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gcfg(){
	git config $@
}