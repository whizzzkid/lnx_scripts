# Check out to main branch

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0

# $1       new branch name
# $2       (optional) commit hash; when not provided, latest commit of currently checked out branch is used

function gcom(){
	main_branch=$(git branch -vv | grep --color=auto -Po "^[\s\*]*\K[^\s]*(?=.*$(git branch -r | grep -Po "HEAD -> \K.*$").*)")
	git checkout $main_branch
}