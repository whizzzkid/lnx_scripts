# This function removes the specified branch locally and remotely

# author:  Tukusej's Sirs
# date:    13 January 2020
# version: 1.1

# $1       existing branch name

function gbd(){
	git branch --delete $1
	git push origin --delete $1 &>/dev/null || git fetch -p
}