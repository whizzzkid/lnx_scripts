# Output remote HEAD (main) branch name

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gbmr(){
	git branch -r | grep -Po 'HEAD -> \K.*$'
}