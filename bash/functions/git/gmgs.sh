# Merge branch $1 without commit history and without commiting anything

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gmgs(){
	git merge --squash $@
}