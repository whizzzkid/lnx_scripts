# Show the working tree status

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gs(){
	git status $@
}