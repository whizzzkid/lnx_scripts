# This function outputs remote branch name that is tracked by current branch

# author:  Tukusej's Sirs
# date:    21 August 2019
# version: 1.0


function gbcr(){
	git for-each-ref --format='%(upstream:short)' $(git symbolic-ref -q HEAD)
}