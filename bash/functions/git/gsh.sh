# Set origin's HEAD

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gsh(){
	git remote set-head origin -a $@
}