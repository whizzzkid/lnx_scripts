# Show differences between HEAD files and files in CWD; if any arg specified, show only those files

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gdiff(){
	git diff $@
}