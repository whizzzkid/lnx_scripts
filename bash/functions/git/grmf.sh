# Resets the file both in the index and in the working tree

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function grmf(){
	git checkout -- $@
}