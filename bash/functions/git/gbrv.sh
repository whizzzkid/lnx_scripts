# Without any additional arg/options, list all remote branches verbously

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gbrv(){
	git branch -rv $@
}