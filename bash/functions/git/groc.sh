# This function replaces `origin` remote with $1, sets current branch's upstream to `origin/$cur_branch` and sets push upstream origin

# Name: Git Remote Origin Change

# author:  Tukusej's Sirs
# date:    05 May 2019
# version: 1.1

function groc(){
	cur_branch=$(git rev-parse --abbrev-ref HEAD)
	origin="$1"

	git remote rm origin
	git remote add origin "$origin" &>/dev/null
	git branch --set-upstream-to=origin/"$cur_branch" "$cur_branch"
	git push --set-upstream origin "$cur_branch"
	git remote set-head origin -a
}