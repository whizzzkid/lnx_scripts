# Set $1 as git global email address

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gcfe(){
	git config --global user.email $@
}