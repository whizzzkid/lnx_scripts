# This function makes a GPG-signed tag, using the default e-mail address’s key and use the given tag message.

# author:  Tukusej's Sirs
# date:    4 August 2019
# version: 1.1
# $1       tag name
# $2       tag message
# $3       commit hash (optional)


function gts(){
	if [ $# <= 2 ]; then
		git tag -s "$1" -m "$(echo -e \"$2\" | sed 's/^"//;s/"$//' -)"
		git push origin --tags
	elif [ $# = 3 ]; then
		git checkout $3
		GIT_COMMITTER_DATE="$(git show --format=%aD | head -1)" git tag -s "$1" -m "$(echo -e \"$2\" | sed 's/^"//;s/"$//' -)"
		git checkout master
		git push origin --tags
	fi
}