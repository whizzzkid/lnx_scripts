# Remove changes (selected files/folders) from stage, i.e. the files become unstaged

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function grmc(){
	git rm --cached -- $@
}