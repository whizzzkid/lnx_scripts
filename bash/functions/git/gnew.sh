#!/bin/bash

# This function create a new remote repo from cwd repo at GitLab.com or at git.itens.sk (server is automatically determined by the namespace)
# Note that currently the following is hard-coded:
# - main brach is called `master`;
# - first commit message is `Initial commit`;
# - remote reference is called `origin`

# author:  Tukusej's Sirs
# date:    19 January 2020
# version: 1.12

# Usage:   gnew [options] namespace/project

# TODO
# - rename `gnew` to something else
# - let the function not only _create_ new repo, but also _update_ some information
# - rename all variables to uppper-case
# - let the user set project AVATAR_URL (subtask: check if I can use custom URL, too), later also group and user AVATAR_URL
# - add short and long help message
# - add an option to change the name of the remote (default: origin)
# - add an option to set custom intial commit message (default: Initial commit)
# - when MASTER_BRANCH is different from master, the local branch name should be renamed too


function gnew(){
	# Default options
	AUTO_DEVOPS='false'
	DESCRIPTION=''
	MAIN_BRANCH='master'
	BW_SESSION="$(cat ${HOME}/.bw/BW_SESSION)"
	REPO_ROOT="$(realpath .)"
	REMOTE_NAME='origin'
	# TAG_LIST=''
	# AVATAR_URL=''
	# ARCHIVED=''
	# VISIBILITY=''
	# ISSUES_ENABLES=''
	# MERGER_REQUESTS_ENABLED=''
	# WIKI_ENABLED=''
	# JOBS_ENABLED=''
	# SNIPPET_ENABLED=''
	# SHARED_RUNNERS_ENABLED=''
	# LFS_ENABLED=''
	# MIRROR=''

	# Call getopt to validate the provided input.
	OPTIONS=$(getopt -o d:a:m:hr: --long description:,auto_devops:,main_branch:,help,repo_root: -- "$@")

	# Short help message
	[ $? -eq 0 ] || {
		echo "ERROR: Invalid options provided." 1>&2
		return 1
	}
	eval set -- "$OPTIONS"
	while true; do
		case "$1" in
			--description|-d)
				shift  # The arg is next in position args
				DESCRIPTION="$1"
			;;
			--main_branch|-m)
				shift
				MAIN_BRANCH="$1"
			;;
			--auto_devops|-a)
				shift
				case "$1" in
					'true'|'TRUE'|'0'|'yes'|'YES')
						AUTO_DEVOPS='true'
					;;
					'false'|'FALSE'|'1'|'no'|'NO')
						AUTO_DEVOPS='false'
					;;
					*)
						echo "ERROR: $1 is not a valid value for auto_devops. Valid values are either true of false." 1>&2
						return 2
					;;
				esac
			;;
			--repo_root|-r)
				shift
				if [ -d "$1" ]; then
					REPO_ROOT="$(realpath $1)"
				else
					echo "ERROR: $1 does not exist or is not a directory." 1>&2
					return 3
				fi
			;;
			--help|-h)
				# Show long help message
				# TODO
				echo "Usage: ${FUNCNAME[0]} [-d | --description] [-a | --auto-devops] [-m | --main_branch] [-h | --help] [-r | repo_root] namespace/project"
				return 0
			;;
			--)
				shift
				OPT_REST="$@"
				break
			;;
		esac
		shift
	done

	# Test if we can access the Bitwarden vault
	if [ "$BW_SESSION" ]; then
		bw_test="$(node /git/others/bw/build/bw.js list items --pretty --session BW_SESSION 2>/dev/null)"
	fi

	if [ ! "$bw_test" ]; then
		echo "WARNING: We could not access Bitwarden vault, therefore we won't be able to use GitLab API." 1>&2
	fi

	namespace="$(echo $1 | grep -oP "^.*(?=/)")"
	case "$namespace" in
		'itens/controlserver'|'itens/control_server'|'controlserver'|'control_server'|'i/c'|'i/cs'|'cs')
			namespace='control_server'
			server='git.itens.sk'            ;;
		'itens/zutom'|'zutom'|'i/z')
			namespace='zutom'
			server='git.itens.sk'            ;;
		'itens/intra'|'intra'|'i/i')
			namespace='intra'
			server='git.itens.sk'            ;;
		'mrtvamanzelka'|'mm')
			namespace='mrtvamanzelka'
			server='gitlab.com'              ;;
		'ofcl')
			namespace='ofcl'
			server='gitlab.com'              ;;
		'ofcl/jobs'|'jobs'|'o/j')
			namespace='ofcl/jobs'
			server='gitlab.com'              ;;
		'os_backups'|'os'|'os_bu'|'osbu')
			namespace='os_backups'
			server='gitlab.com'              ;;
		'os_backups/apps'|'os/apps'|'os_bu/apps'|'osbu/apps'|'apps')
			namespace='os_backups/apps'
			server='gitlab.com'              ;;
		'os_backups/e531'|'os/e531'|'os_bu/e531'|'osbu/e531'|'e531')
			namespace='os_backups/e531'
			server='gitlab.com'              ;;
		'os_backups/domcek_forpsi'|'os/domcek_forpsi'|'os_bu/domcek_forpsi'|'osbu/domcek_forpsi'|'domcek_forpsi')
			namespace='os_backups/domcek_forpsi'
			server='gitlab.com'              ;;
		'ts_network'|'net'|'network'|'nw')
			namespace='ts_network'
			server='gitlab.com'              ;;
		'romlit')
			namespace='romlit'
			server='gitlab.com'              ;;
		'scanned_books'|'sbks'|'sb')
			namespace='scanned_books'
			server='gitlab.com'              ;;
		'ts'|'tukusejssirs')
			namespace='tukusejssirs'
			server='gitlab.com'              ;;
		*)
			echo "ERROR: Namespace $namespace is not recognised." 1>&2
			return 4
		;;
	esac

	project=$(echo $1 | grep -oP "^[^/]*/\K.*$")

	# Get token if we can access the Bitwarden vault
	if [ "$bw_test" ]; then
		case $server in
			gitlab.com)
				token=$(node /git/others/bw/build/bw.js get item "71a20424-7c68-4506-8ee1-aaf900d4d4d0" --session $BW_SESSION | jq -r .notes)
			;;
			git.itens.sk)
				token=$(node /git/others/bw/build/bw.js get item "1d62f78b-fab0-4cef-87e8-aafd01559445" --session $BW_SESSION | jq -r .notes)
			;;
		esac
	fi

	if [ -d "$REPO_ROOT/.git" ]; then
		echo "WARNING: Repository in $REPO_ROOT is already initialised." 1>&2

		# Check if everything is committed locally
		if [ "$(git status -s)" ]; then
			echo 'ERROR: As the repository is already initialised, you should commit all the changes before you create the remote repository.' 1>&2
			return 5
		fi
	else
		# Initialise local Git repository
		git init

		# Create initial commit
		git add --all && git commit -am 'Initial commit'
	fi

	# Create remote repository
	git push --set-upstream git@$server:$namespace/$project.git $MAIN_BRANCH
	git remote add $REMOTE_NAME git@$server:$namespace/$project.git
	git fetch $REMOTE_NAME $MAIN_BRANCH
	git remote set-head $REMOTE_NAME -a


	if [ "$token" ]; then
		# Get namespace ID when $namespace is a username
		namespace_id="users/$(curl -s "https://$server/api/v4/users/?username=$namespace&private_token=$token" | jq -r .[].id)"

		# Get namespace ID when $namespace is a (sub)group name
		if [ "$namespace_id" = 'users/' ]; then
			if [ "$(grep -o '/' <<< $namespace)" = '/' ]; then
				subnamespace="$(grep -Po '/\K[^/]*$' <<< "$namespace")"
				namespace_id="groups/$(curl -s "https://$server/api/v4/groups/?search=$subnamespace&private_token=$token" | jq -r --arg namespace "$namespace" '.[] | select(.full_path == $namespace).id')"
			else
				namespace_id=groups/$(curl -s "https://$server/api/v4/groups/?search=$namespace&private_token=$token" | jq -r .[].id)
			fi
		else
			echo "ERROR: We could not find $namespace namespace at $server." 1>&2
		fi

		# Remote project ID
		project_id=$(curl -s https://$server/api/v4/$namespace_id/projects?private_token=$token | jq -r --arg path_with_namespace "$namespace/$project" '.[] | select(.path_with_namespace == $path_with_namespace).id')

		# Update the project (repository) description
		curl -sH "Content-Type: application/json" -X PUT --data "{\"description\":\"$DESCRIPTION\", \"auto_devops_enabled\":\"$AUTO_DEVOPS\", \"default_branch\":\"$MAIN_BRANCH\"}" https://$server/api/v4/projects/$project_id?private_token=$token
	fi
}