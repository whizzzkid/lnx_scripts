# This function sets up LFS files tracking.

# src: https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html

# author:  Tukusej's Sirs
# date:    14 July 2019
# version: 1.0

# $@       files or patterns that should be tracked by LFS

function glfs(){
	# User-dependent variables
	git_repo_root=$(git rev-parse --show-toplevel)
	tmp=/dev/shm

	# Initiate LFS
	git lfs install

	# Set up LFS files tracking
	for n in "$@"; do
		git lfs track "${n}"
		echo "${n}" filter=lfs diff=lfs merge=lfs -text >> ${git_repo_root}/.gitattributes
	done

	# Sort and uniq .gitattributes
	cat ${git_repo_root}/.gitattributes | sort | uniq > ${tmp}/.gitattributes
	cp ${tmp}/.gitattributes ${git_repo_root}/.gitattributes
}