#!/bin/bash

# This script logs into Bitwarden and unlocks the session

# author:  Tukusej's Sirs
# date:    2 November 2019
# version: 1.0

# $1       email address (optional; required when you want to use second argument as password)
# $2       password (optional)


function bli() {
	# Variables
	email="$1"
	if [ "$2" ]; then
		password="$2"
	else
		# TODO: This does not work yet
		read -sp "? Master password: [input is hidden] " password
		password="$(sed -z 's/\n//g' <<< "$password")"
	fi

	bw_session_file="${HOME}/.bw/BW_SESSION"
	mkdir -p ${HOME}/.bw

	if [ ! $BW_SESSION ] && [ -f "$bw_session_file" ]; then
		BW_SESSION=$(cat $bw_session_file)
	fi

	test_login=$(bw login --check &>/dev/null; echo $?)
	if [ $test_login -gt 0 ]; then  # We need to login
		BW_SESSION=$(bw login $email "$password" | tail -1 | awk '{print $6}')
	else  # We are logged in
		test_lock=$(test $BW_SESSION && bw unlock --check --session $BW_SESSION &>/dev/null; echo $?)
		case $test_lock in
			1)
				# We unlock the vault a get a new session key
				BW_SESSION=$(bw unlock "$password" | tail -1 | awk '{print $6}')
			;;
		esac
	fi

	# Save the BW_SESSION key to file
	echo $BW_SESSION > "$bw_session_file"
}