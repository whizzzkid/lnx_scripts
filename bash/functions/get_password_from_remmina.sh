# This script recovers pass saved in remmina

# author:  Tukusej's Sirs
# date:    29 August 2019
# version: 1.0

# src: https://stackoverflow.com/a/56053949/3408342

# TODO
# - make a function from this script

# Dependencies for CentOS 7.4
# Python 3.6.6 : python36-devel
# Python 2     : python-devel

# python 3+
python_version=$(python --version 2>&1 | grep -Po "^Python \K[0-9]")
if [ $python_version = 3 ]; do  # python 3+; not tested
	sudo yum -y install python2-pip python34-devel
} elif [ $python_version = 2 ]; do  # python 2+; tested and works
	sudo yum -y install python2-pip python-devel
fi

sudo pip install --upgrade pip
sudo pip install pycrypto

python -c "import base64,sys;from Crypto.Cipher import DES3;pc=open(sys.argv[1]).read();pci=pc.index('secret=');secret=pc[pci:pc.index('\n',pci)].split('=',1)[1];cc=open(sys.argv[2]).read();cci=cc.index('password');password=cc[cci:cc.index('\n',cci)].split('=',1)[1];secret,password=base64.decodestring(secret),base64.decodestring(password); print DES3.new(secret[:24], DES3.MODE_CBC, secret[24:]).decrypt(password)" ${HOME}/.remmina/remmina.pref ${HOME}/.remmina/*.remmina