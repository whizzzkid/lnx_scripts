# This function OCR's a PDF file page-by-page. Each page be be in a separate TXT file.

# author:  Tukusej's Sirs
# date:    20 October 2019
# version: 1.3

# $1       [path/]pdf_file
# $2       [path/]output_file
# $3       [lang] (default: eng; multiple languages may be specified, separated by plus characters)
# $4       [start_page] (default: first page)
# $5       [last_page] (default: last page)
# $6       [prefix_width] (default: same as the width of the total page number)

# If you want to skip an argument, use a hyphen (-). This is only applicable to arguments $3 through $6.

# Return codes:
# 0        Success
# 1        Input file does not exist
# 2        Output file path does not exist
# 30+n     Tesseract command was unsuccessful n times

# pdfocr [path/]pdf_file [path/]output_file lang [leading_number_width] [start_page] [last_page]

# TODO
# - use `getopts` for options gathering
# - option: add separate output path for images and for text files

function pdfocr() {
	# Check input file and its path
	if [ "$1" != "" ] && [ -e $1 ]; then
		local input_path=$(dirname $(realpath $1))
		local input_file=$(basename $(realpath $1))
	else
		echo "ERROR: File $1 does not exist."
		exit 1
	fi

	# Check output file and its path
	if [ -e $(dirname $(realpath $2)) ]; then
		local output_path=$(dirname $(realpath $2))
		local output_file=$(basename $(realpath $2))
	else
		echo "ERROR: Output file path $(dirname $(realpath $2)) does not exist."
		exit 2
	fi

	# OCR language(s)
	if [ "$3" = "-" ]; then
		local lang="eng"
	else
		local lang="$3"
	fi

	# Start page
	if [ "$4" = "-" ]; then
		local start_page="1"
	else
		local start_page="$4"
	fi

	# Last page
	if [ "$5" = "-" ]; then
		local last_page=$(pdfinfo ${input_path}/${input_file} | grep -Po "^Pages:\s*\K[0-9]*")
	else
		local last_page="$5"
	fi

	# Prefix width
	if [ "$6" = "-" ]; then
		local prefix_width="${#last_page}"
	else
		local prefix_width="$6"
	fi

	for n in $(seq $start_page $last_page); do
		local output_file_name="$(printf "%0*d" "${prefix_width}" ${n})_${output_file}"
		echo -n "Processing file $output_file_name ($n of $last_page) ... "
		pdfimages -png -f ${n} -l ${n} "${input_path}/${input_file}" "${output_path}/${output_file_name}"
		mv ${output_path}/$output_file_name-*.png "${output_path}/$output_file_name.png"
		tesseract ${output_path}/{output_file_name}* "${output_path}/${output_file_name}" -l ${lang} 2>/dev/null
		local test=$?
		if [ $test > 0 ]; then
			if [ "$tess_success" = "" ]; then
				local tess_success=30
			fi
			let tess_success+=1
		fi

		echo "done!"
	done

	if [ "$tess_success" != "" ]; then
		exit $tess_success
	fi
}