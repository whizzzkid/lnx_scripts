#!/bin/bash

# Upgrade script to upgrade system packages, Git repositories (some with building and installation)

# author:  Tukusej's Sirs
# date:    7 January 2020
# version: 0.1


# Upgrade all packages from all enabled repos
sudo dnf -y upgrade

# Update Git repositories
# TODO: USE XDG_GIT*_DIR vars
git -C /git/others/libertinus pull
git -C /git/others/dotfiles pull
git -C ~/git/itens/intra/knowbase.wiki pull

# Upgrade Git from source
# TODO: Check if there is something to upgrade
old_path="$PWD"
cd "/git/git"
git pull
latest_version="$(git tag | sort -V | sed '/rc/d' | tail -1)"
current_version="v$(git --version | grep -o '[0-9.]*$')"

if [ "$latest_version" != "$current_version" ]; then
	# Check out the latest stable version of Git
	git checkout "$latest_version"

	# Clean the repo (just to make sure)
	make clean

	# Configure Git
	make configure
	./configure --prefix=/usr

	# Build and install Git (including docs)
	# Note on `NO_SVN_TESTS=1`: I skip SVN tests because:
	#   (1) I don’t use SVN nor `git-svn`;
	#   (2) those tests take up a significant amount of the total test time and are not needed unless you plan to talk to SVN repos (according to the Git’s `Makefile`);
	# Note on `GIT_SKIP_TESTS='t9020'`: On CentOS 8 some of these tests (t9020-remote-svn.sh: test 1, 2, 3, 4, 6) have failed me on 6 Jan 2020, so skipped the whole `t9020-remote-svn.sh` test altogether (again: because I don’t use `git-svn` at all).
	# Note on `DEFAULT_EDITOR='/usr/bin/nano'`: I have set `nano` as the default editor for Git.
	# Note on `profile`: This option makes `make` build Git much longer, but should make it ‘a few percent faster on CPU intensive workloads’ (according to Git’s `INSTALL`).
	# Note: The following command took about 4 hours (while doing some other stuff) on Lenovo E531 with Intel(R) Core(TM) i5-3230M CPU @ 2.60GHz and 12 GB RAM (of which 2 GB is used by Intel graphics) using 3 cores (`-j3`)
	NO_SVN_TESTS=1 GIT_SKIP_TESTS='t9020' DEFAULT_EDITOR="$(which nano)" make -j4 profile all doc info
	# NO_SVN_TESTS=1 GIT_SKIP_TESTS='*svn*' DEFAULT_EDITOR='/usr/bin/nano' make -j4 profile all doc info
	sudo bash -c "cd /git/git; NO_SVN_TESTS=1 GIT_SKIP_TESTS='t9020' DEFAULT_EDITOR=\"$(which nano)\" make PROFILE=BUILD install install-doc install-html install-info"

	current_version="v$(git --version | grep -o '[0-9.]*$')"

	if [ "$latest_version" = "$current_version" ]; then
		make clean

		# Check out the main branch
		main_branch=$(git branch -vv | grep --color=auto -Po "^[\s\*]*\K[^\s]*(?=.*$(git branch -r | grep -Po "HEAD -> \K.*$").*)");
		git checkout $main_branch
	else
		echo "ERROR: Something went wrong.\nLatest version  : $latest_version\nCurrent version : $current_version" 2&>1
	fi
fi

# Upgrade Sublime Text 3
subl_ver_prev=$(subl --version | grep -o '[0-9]*$')

sudo dnf -y install sublime-text

# Get the currently installed version
subl_ver=$(subl --version | grep -o '[0-9]*$')

if [ "$subl_ver" != "$subl_ver_prev" ]; then
	if [ ! "$(grep '^[0-9]*$' <<< "$subl_ver")" ]; then
		sudo dnf -y remove sublime-text
		sudo dnf -y install sublime-text
		subl_ver=$(subl --version | grep -o '[0-9]*$')
	fi

	url="https://cynic.al/warez/$(curl -s https://cynic.al/warez/ | grep -o "sublime-text/${subl_ver}-linux/sublime_text_linux${subl_ver}cracked")"
	mv /opt/sublime_text/sublime_text{,${subl_ver}.bak}
	curl -so /opt/sublime_text/sublime_text "$url"
	cp /opt/sublime_text/sublime_text /opt/sublime_text/sublime_text.${subl_ver}.mdfd
fi

# Change the CWD back
if [ "$PWD" != "$old_path" ]; then
	cd "$old_path"
fi