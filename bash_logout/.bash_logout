# This command saves only unique commands in ~/.bash_history_ts without sorting,
# while leaving only the command that was run most recently
tac ${HOME}/.bash_history_ts | cat -n | sort -uk2 | sort -nk1 | cut -f2- | tac > ${HOME}/.bash_history_ts