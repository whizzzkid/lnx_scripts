# TODO
# - git stuff update scripts [1]
# - .bash_history (auto-update and git push to lnx_*)
# - ls aliases
# - git add + git commit + git push
# - megasync
# - check basic bash settings like shopt (etc)
# - add path to PS1
# - check if terminal supports colours (in bash/functions/bash_colours.sh)
# - prompt: if logged in locally:  [user of  local host]@[local  host]
# - prompt: if logged in remotely: [user of remote host]@[remote host]
# - create separate file for paths which would check for there existance first

# [1]
# # update local romcal git repo
# p=$PWD
# cd $HOME/.scripts/romcal
# git fetch && git pull origin && git pull
# cd $p

# Source ${HOME}/.config/user-dirs.dirs to be able to use the variables in shell
source ${HOME}/.config/user-dirs.dirs

# In case `user-dirs.dirs` does not define XDG_GIT_DIR variable
if [ ! "$XDG_GIT_DIR" ]; then
	XDG_GIT_DIR='/git'
fi

# Source `src` function
source ${XDG_GIT_DIR}/lnx_scripts/bash/functions/src.sh

# Check where we are running the shell (Android/Termux, Linux)
# Then set some system-dependant variables
if [[ $(uname -o) == "Android" ]]; then
	if [[ $(echo $SHELL) == "/data/data/com.termux/files/usr/bin/login" ]] || [[ $(echo $SHELL) == "/data/data/com.termux/files/usr/bin/bash" ]]; then
		# We are in Android/Termux
		usr="/data/data/com.termux/files/usr"
		etc="/data/data/com.termux/files/usr/etc"
		bin="/data/data/com.termux/files/usr/bin"
		var="/data/data/com.termux/files/usr/var"
		dev="/dev"

		# To make `su` work, add `/su/bin/` to $PATH
		PATH=/su/bin:/data/data/com.termux/files/usr/bin:/data/data/com.termux/files/usr/bin/applets

		# Android/Termux-specific aliases
		# apt aliases
		alias upgrade="apt-get -yq update && apt-get -yq --with-new-pkgs upgrade"
		alias install="apt-get -yq install"
		alias ogus="${XDG_GIT_DIR}/lnx_scripts/bash/functions/termux/ogus.sh"

		alias smake="make"
	fi
elif [[ $(uname -o) == "GNU/Linux" ]]; then
	# apt aliases
	alias upgrade="sudo apt-get -yq update && sudo apt-get -yq --with-new-pkgs upgrade"
	alias install="sudo apt-get -yq install"

	usr="/usr"
	etc="/etc"
	bin="/bin"
	var="/var"
	dev="/dev"

	# Linux-specific aliases
	alias smake="sudo make"

	# Linux-specific functions
	src $path_fn/brightness.sh

	if [[ $(uname -r | grep -o "Microsoft$") == "Microsoft" ]]; then
		# Support of Linux permissions in the WSL
		#cwd=$PWD
		# cd /  # Change to dir outside /mnt/c (or the symlink to it)
		# sudo umount /mnt/c && sudo mount -t drvfs C: /mnt/c -o metadata  # Remount /mnt/c with linux perms support
		# cd $cwd  # Enter the home dir when started

		# TODO:
		# - make $PATH here as it is not set the same way for root user as it is for regular user

		src $path_fn/wsl/win_progs.sh
	fi
fi

# $HOME/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# Eternal bash history
# --------------------
# http://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=${HOME}/.bash_history_ts
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
# NOTE: this for a reason unknown to me does not work on CentOS 7 (tested on bash 4.2 and 5)
# Setting HISTSIZE to a value less than zero causes the history list to be unlimited (setting it 0 zero disables the history list).
# Setting HISTFILESIZE to a value less than zero causes the history file size to be unlimited (setting it to 0 causes the history file to be truncated to zero size).
#HISTSIZE=-1
#HISTFILESIZE=-1

# Check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will match all files and zero or more directories and subdirectories.
shopt -s globstar

# Make less more friendly for non-text input files, see lesspipe(1)
[ -x $usr/bin/lesspipe ] && eval "$(SHELL=$bin/sh lesspipe)"

# Coloured GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

if [ -n "$force_color_prompt" ]; then
	if [ -x $usr/bin/tput ] && tput setaf 1 >&$dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
		color_prompt=yes
	else
		color_prompt=
	fi
fi

# Add an "alert" alias for long running commands. Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Files sourcing ##############################################################

# See $usr/share/doc/bash-doc/examples in the bash-doc package.
### Interactive ### {{{
if [[ $- == *i* ]]; then
	path_fn="${XDG_GIT_DIR}/lnx_scripts/bash/functions"

	# Bash colours
	src $path_fn/bash_colours.sh

	# Bash aliases
	src ${XDG_GIT_DIR}/lnx_scripts/bash/aliases/.bash_aliases

	# Git functions
	src $path_fn/git

	# Bash functions
	src $path_fn/add_ssh_keys.sh
	src $path_fn/bli.sh
	src $path_fn/blo.sh
	src $path_fn/bs.sh
	src $path_fn/bsl.sh
	src $path_fn/bscan.sh
	src $path_fn/cconv.sh
	src $path_fn/char_multiplier.sh
	src $path_fn/create_ssh_keys.sh
	src $path_fn/cue2cd.sh
	src $path_fn/fram.sh
	src $path_fn/gls.sh
	src $path_fn/gshort.sh
	src $path_fn/hexcol.sh
	src $path_fn/mkcd.sh
	src $path_fn/pdfocr.sh
	src $path_fn/prompt.sh
	src $path_fn/prosby.sh
	src $path_fn/rawurldecode.sh
	src $path_fn/rawurlencode.sh
	src $path_fn/round.sh
	src $path_fn/scan.sh
	src $path_fn/ssh_key_init.sh
	src $path_fn/termtitle.sh
	src $path_fn/up.sh

	# Bash programs
	# src $HOME/.bash_progs
fi

# Enable programmable completion features
# Note: You don't need to enable this, if it's already enabled in $etc/bash.bashrc and $etc/profile sources /etc/bash.bashrc.
if ! shopt -oq posix; then
  if [ -f $usr/share/bash-completion/bash_completion ]; then
	src $usr/share/bash-completion/bash_completion
  elif [ -f $etc/bash_completion ]; then
	src $etc/bash_completion
  fi
fi

# SSH identities
if [ "$(ps aux | grep [s]sh-agent)" ]; then
	pkill ssh-agent
fi

eval $(ssh-agent -s) &> /dev/null

for n in $(ls ${HOME}/.ssh/*.pub 2>/dev/null | sed 's/\.pub//g' -); do
	ssh-add $n &> /dev/null
done

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="${PATH}:${HOME}/.rvm/bin"
[[ -s "${HOME}/.rvm/scripts/rvm" ]] && source "${HOME}/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Add `/opt/bin` to PATH
if [ -d /opt/bin ]; then
	export PATH="$PATH:/opt/bin"
fi

# Add TexLive 2019 to PATH
if [ -e /usr/local/texlive/2019/bin/x86_64-linux ]; then
	export PATH="$PATH:/usr/local/texlive/2019/bin/x86_64-linux"
fi

# Add TexLive 2019 to MANPATH.
if [ -e /usr/local/texlive/2019/texmf-dist/doc/man ]; then
	export MANPATH="$MANPATH:/usr/local/texlive/2019/texmf-dist/doc/man"
fi

# Add TexLive 2019 to INFOPATH.
if [ -e /usr/local/texlive/2019/texmf-dist/doc/info ]; then
	export INFOPATH="$INFOPATH:/usr/local/texlive/2019/texmf-dist/doc/info"
fi

# perl/cpan paths
# TODO: change `${HOME}/perl5` to `${XDG_PROG_BIN_DIR}/perl5`
export PERL_LOCAL_LIB_ROOT="$PERL_LOCAL_LIB_ROOT:${HOME}/perl5"
export PERL_MB_OPT="--install_base ${HOME}/perl5"
export PERL_MM_OPT="INSTALL_BASE=${HOME}/perl5"
export PERL5LIB="${HOME}/perl5/lib/perl5:$PERL5LIB"
export PATH="${HOME}/perl5/bin:$PATH"

trap_exit() {
	if [ -e "${HOME}/.bash_logout" ]; then
    . "${HOME}/.bash_logout"
  fi
}
trap trap_exit EXIT
export NVM_DIR="${HOME}/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# to use gcc v4.9 on centos 7
# scl enable devtoolset-3 bash

# Export BASH_ENV in order to source ${HOME}/.bashrc when running a non-interactive shell (e.g. in scripts)
# I use it e.g. to use the variables set here in a particular script
export BASH_ENV="${HOME}/.bashrc"

# This command runs (let’s say) a `bw` agent
# npm run --prefix $XDG_GIT_DIR/others/bw build:watch &>/dev/null& disown
# $BW_WATCH_ID=$(jobs -p)

# This is needed at least on CentOS Stream 8; otherwise `gpg2` would fail (`error: gpg failed to sign the data`)
export GPG_TTY=$(tty)

# Set Bitwarden session ID if available
if [ -f ${HOME}/.bw/BW_SESSION ]; then
	export BW_SESSION="$(cat ${HOME}/.bw/BW_SESSION)"
fi

# Run start up script
if [ -e "$(which git)" ] && [ -e "$path_fn/startup_script.sh" ]; then
	sh "$path_fn/startup_script.sh"
fi