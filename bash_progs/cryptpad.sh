#!/bin/bash

# This function install `cryptpad`

# author:  Tukusej's Sirs
# date:    2 September 2019
# version: 0.2


# Install dependencies
sudo yum -y install git nodejs
curl -so- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

# Clone cryptpad repo
mkdir -p ${XDG_GIT_DIR}/cryptpad
git clone https://github.com/xwiki-labs/cryptpad.git ${XDG_GIT_DIR}/cryptpad

# Install cryptpad
cd ${XDG_GIT_DIR}/cryptpad
npm install node bower
npm install
# bower install  # this does not work for some reason
node_modules/bower/bin/bower install  # this works

# Configure cryptpad
cp ${XDG_GIT_DIR}/cryptpad/config/{config.example.js,config.js}

# Run cryptpad
node server