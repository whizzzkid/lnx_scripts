#!/bin/bash

# Install Docker

# author:  Tukusej's Sirs
# date:    22 December 2019
# version: 1.8

# src: https://docs.docker.com/install/linux/docker-ce/centos/


# Remove previous versions of these packages
sudo yum -y remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine

# Install required packages
# `yum-utils` provides the `yum-config-manager` utility
# `device-mapper-persistent-data` and `lvm2` are required by the `devicemapper` storage driver
sudo yum -y install yum-utils device-mapper-persistent-data lvm2

# Add the Docker stable repository
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo


# Install the latest version of Docker Engine CE and `containerd`version:
sudo yum -y install docker-ce docker-ce-cli containerd.io

# Enable and start the `docker` service
sudo systemctl enable docker
sudo systemctl start docker