#!/bin/bash

# This script installs sane-backends from source

# author:  Tukusej's Sirs
# date:    2 September 2019
# version: 1.1

# Not working yet, at least not on CentOS 7

# Dependencies: Requires `automake_src` and `libtool_src` to be installed in advance


mkdir -p ${XDG_GIT_DIR}/libtool
sudo yum -y install libusb-devel libjpeg-turbo-devel libpng-devel libtool-ltdl-devel autoconf-archive texinfo
git clone git://git.savannah.gnu.org/libtool.git ${XDG_GIT_DIR}/libtool
cd ${XDG_GIT_DIR}/libtool
./bootstrap
./configure
make && sudo make install
git clone https://gitlab.com/sane-project/backends.git ${XDG_GIT_DIR}/sane_backends
cd ${XDG_GIT_DIR}/sane_backends
./autogen.sh