#!/bin/bash

# Install Sublime Merge and crack it

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


# Add repos
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

# Install Sublime Merge
sudo dnf -y install sublime-merge

# Crack Sublime Merge
smerge_ver=$(smerge --version | grep -o '[0-9]*$')
url="https://cynic.al/warez/$(curl -s https://cynic.al/warez/ | grep -o "sublime-merge/${smerge_ver}-linux/sublime_merge_linux${smerge_ver}cracked")"
mv /opt/sublime_merge/sublime_merge{,.bak}
curl -so /opt/sublime_merge/sublime_merge "$url"
# It might be required to 're-crack' it after upgrade
cp /opt/sublime_merge/sublime_merge /opt/sublime_merge/sublime_merge.mdfd
# Install the dummy licence
curl -so ~/.config/sublime-merge/Local/License.sublime_license https://cynic.al/warez/sublime-merge/sm_dummy_licence.txt