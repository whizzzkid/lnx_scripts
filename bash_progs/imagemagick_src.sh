#!/bin/bash

# This script builds and installs the latest version of ImageMagick from source with all the available delegates

# author:  Tukusej's Sirs
# date:    23 December 2019
# version: 1.2

# TODO
# - create a script where the user could select which delegates to install
#   - there would be the base dependencies and per-delegate dependencies

# Currently, the following delegates are no installable:
	# Delegate library configuration:
	#   DJVU              --with-djvu=yes             no
	#   DPS               --with-dps=yes              no
	#   FLIF              --with-flif=yes             no
	#   FlashPIX          --with-fpx=yes              no
	#   Ghostscript lib   --with-gslib=yes            no
	#   JPEG XL           --with-jxl=yes              no
	#   LQR               --with-lqr=yes              no
	#   LTDL              --with-ltdl=yes             no


# Variables
build_dir="${XDG_GIT_DIR}/others/imagemagick"

# Install the dependencies (some are optional)
sudo dnf -y install https://rpms.remirepo.net/enterprise/remi-release-$(rpm -E %centos).rpm  # Needed for `libraqm*`
sudo dnf -y install bzip2-devel libzstd libzstd-devel fftw-libs fftw-devel libheif libheif-devel jbigkit jbigkit-devel libjpeg libjpeg-devel lcms2 lcms2-devel OpenEXR OpenEXR-devel pango-devel libpng libpng-devel LibRaw LibRaw-devel libtiff libtiff-devel libwebp libwebp-devel libwmf libwmf-devel openjpeg2-devel-2.3.0 openjpeg2-2.3.0 openjpeg2-tools-2.3.0 librsvg2 librsvg2-devel graphviz graphviz-devel libtool-ltdl libtool-ltdl-devel libraqm libraqm-devel

# Clone the repo
mkdir -p "${build_dir}"
git clone git@github.com:ImageMagick/ImageMagick.git "${build_dir}"
cd "${build_dir}"

# Get latest release number
ver_lat="$(git tag | sed '/continuous/d' | sort -V | tail -1)"

# Check out to the latest version
git checkout $ver_lat &/dev/null

# Configure the build
./configure --enable-shared --with-fpx --with-gslib --with-fontpath --with-bzlib --with-rsvg=yes

# Remove previously installed ImageMagick*
sudo dnf -y remove ImageMagick*

# Install ImageMagick
make && sudo make install