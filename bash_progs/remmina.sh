#!/bin/bash

# This script install Remmina

# author:  Tukusej's Sirs
# date:    20 January 2020
# version: 1.3


# Make sure not `remmina` or `freerdp` is installed
sudo yum -y remove freerdp* remmina*

# Add required repos
# epel: required for `openjpeg2-devel`, `openssl-devel`
# rpmfusion-free-updates: required for `ffmpeg-devel`
# PowerTools repo is needed on CentOS 8+
# getpagespeed repo is needed on CentOS 8+: required for `libgnome-keyring-devel`, `libappindicator-devel`
# okay repo is needed on CentOS 8+; required for `avahi-ui-devel`
sudo yum -y install epel-release https://download1.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %centos).noarch.rpm
if [ $(rpm -E %centos) = 8 ]; then
  sudo dnf config-manager --set-enabled PowerTools
  sudo dnf -y install https://extras.getpagespeed.com/release-el$(rpm -E %centos)-latest.rpm
  sudo rpm -ivh http://repo.okay.com.mx/centos/8/x86_64/release/okay-release-1-3.el8.noarch.rpm
fi

# Update yum cache and packages
sudo yum -y upgrade

# Test if `vte3-devel` can be installed
test_vte=$(yum info vte3-devel &>/dev/null; echo $?)

if [ $test_vte = 0 ]; then
	# On CentOS 7- and Fedora XX+
	vte='vte3 vte3-devel'
else
	# On CentOS 8+ and Fedora XX+
	vte='vte291 vte291-devel'
fi

# Cloning dependency
sudo yum -y install git

# FreeRDP dependencies
# TODO: official docs: gcc cmake ninja-build openssl-devel libX11-devel libXext-devel libXinerama-devel libXcursor-devel libXi-devel libXdamage-devel libXv-devel libxkbfile-devel alsa-lib-devel cups-devel ffmpeg-devel glib2-devel libusb-devel
# TODO: add (at least on CentOS 8, probably on CentOS 7 too; otherwise build fails): make gcc-c++ pulseaudio-libs-devel libXrandr-devel
# TODO: recommended (for man pages): libxslt libxslt-devel docbook-style-xsl
# TODO: recommended (for multimedia redirection, audio and video playback): gstreamer1-devel gstreamer1-plugins-base-devel
# TODO: recommended (): xorg-x11-server-utils
# TODO: recommended (required by virtual:world): cairo-devel
# Remmina dependencies (without FreeRDP and its dependencies) (c8)
# cmake3 gtk3-devel libgcrypt-devel libssh-devel libxkbfile-devel openjpeg2-devel gnutls-devel libgnome-keyring-devel avahi-ui-devel avahi-ui-gtk3 libvncserver-devel $vte libappindicator-devel libappindicator-gtk3 libappindicator-gtk3-devel libSM-devel webkitgtk4-devel json-glib-devel libsoup-devel libsodium libsodium-devel libXtst-devel xmlto harfbuzz-devel pango-devel atk-devel libsecret-devel
sudo yum -y install $vte alsa-lib-devel atk-devel avahi-ui-devel avahi-ui-gtk3 cairo-devel cmake3 cups-devel docbook-style-xsl ffmpeg-devel gcc gcc-c++ glib2-devel gnutls-devel gstreamer1-devel gstreamer1-plugins-base-devel gtk3-devel harfbuzz-devel json-glib-devel libappindicator-devel libappindicator-gtk3 libappindicator-gtk3-devel libgcrypt-devel libgnome-keyring-devel libsecret-devel libSM-devel libsodium libsodium-devel libsoup-devel libssh-devel libusb-devel libvncserver-devel libX11-devel libXcursor-devel libXdamage-devel libXext-devel libXi-devel libXinerama-devel libxkbfile-devel libXrandr-devel libxslt libxslt-devel libXtst-devel libXv-devel make ninja-build openjpeg2-devel openssl-devel pango-devel pulseaudio-libs-devel webkitgtk4-devel xmlto xorg-x11-server-utils

# Remove freerdp-x11 package and all packages containing the string remmina in the package name
# Note: no such packages are available
# sudo rpm -e remmina remmina-devel remmina-plugins-gnome remmina-plugins-nx remmina-plugins-rdp remmina-plugins-telepathy remmina-plugins-vnc remmina-plugins-xdmcp

# If you install FreeRDP and Remmina to `/opt`, you need to add `/opt/bin` to PATH
export PATH="$PATH:/opt/bin"
# echo 'export PATH="$PATH:/opt/bin"' >> ${HOME}/.bashrc

# Clone FreeRDP and Remmina repos
mkdir -p ${XDG_GIT_DIR}/{freerdp,remmina}
git clone https://github.com/FreeRDP/FreeRDP.git ${XDG_GIT_DIR}/others/freerdp
git clone https://gitlab.com/Remmina/Remmina.git ${XDG_GIT_DIR}/others/remmina

# Build FreeRDP
mkdir ${XDG_GIT_DIR}/others/freerdp/build
cd ${XDG_GIT_DIR}/others/freerdp/build
# Note: In the following line, the `DWITH_PULSE=ON` option needs to be included
# Note: `-DCMAKE_INSTALL_LIBDIR=/usr/lib64` is required when `-DCMAKE_INSTALL_PREFIX:PATH` is not `/usr`; otherwise Remmina will not find the `libfreerdp*` libraries
cmake3 -DCMAKE_BUILD_TYPE=Debug -DWITH_SSE2=ON -DWITH_PULSE=ON -DWITH_CUPS=on -DWITH_WAYLAND=off -DCMAKE_INSTALL_LIBDIR=/usr/lib64 -DCMAKE_INSTALL_PREFIX:PATH=/opt ..
make && sudo make install

# Make your system dynamic loader aware of the new libraries you installed
sudo ldconfig

# You can test FreeRDP by connecting to an RDP host
#xfreerdp +clipboard /sound:rate:44100,channel:2 /v:hostname /u:username

# Build Remmina
mkdir ../../remmina/build
cd ../../remmina/build
# Note: `-DCMAKE_INSTALL_LIBDIR=/usr/lib64` is not required to successfully run Remmina, but `/usr/lib64` is the proper location for the libraries; again, it is not required at all when Remmina is installed to `/usr`
cmake3 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=/opt -DCMAKE_INSTALL_LIBDIR=/usr/lib64 -DCMAKE_PREFIX_PATH=/usr --build=build ..
make && sudo make install

# Unistall
# sudo rm /etc/ld.so.conf.d/freerdp_devel.conf /usr/local/bin/remmina /usr/local/bin/xfreerdp
# sudo ldconfig
# rm /usr/bin/remmina /usr/lib64/remmina /usr/include/remmina /usr/share/remmina /usr/share/man/man1/remmina.1 /usr/bin/xfreerdp /usr/share/man/man1/xfreerdp.1 /usr/share/man/man1/xfreerdp.1.gz