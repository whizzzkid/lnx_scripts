#!/bin/bash

# Android Platform Tools installation

# author:  Tukusej's Sirs
# date:    17 February 2020
# version: 1.0


# Variables
TEMP='/dev/shm'

# Remove the old installation
rm -rf /opt/android_platform_tools /opt/bin/{adb,fastboot}

# Download the archive
curl -sLo "${TEMP}/platform-tools-latest-linux.zip" "https://dl.google.com/android/repository/platform-tools-latest-linux.zip"

# Decompress the archive
unzip -qq "${TEMP}/platform-tools-latest-linux.zip" -d /opt

# Rename the installation folder
mv /opt/platform-tools /opt/android_platform_tools

# Create symlinks to /opt/bin
ln -s /opt/android_platform_tools/adb /opt/bin/adb
ln -s /opt/android_platform_tools/fastboot /opt/bin/fastboot