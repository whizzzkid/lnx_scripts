#!/bin/bash

# This script installs jbead

# author:  Tukusej's Sirs
# date:    8 January 2020
# version: 1.1


url="https://www.jbead.ch/"$(curl -s https://www.jbead.ch/Download.html | grep -oP "href=\"\K.*linux.tar.gz")
filename=$(echo $url | grep -oP "/\K[^/]*$")
sudo curl -so "/opt/$filename" $url
sudo tar xf "/opt/$filename" -C "/opt/"
sudo rm "/opt/$filename"
sudo mv /opt/jbead* /opt/jbead
sudo ln -s /opt/jbead/jbead /usr/bin/jbead
sudo 7za e /opt/jbead/jbead.jar -o/opt/jbead/ images/jbead-32.png

# Create .desktop file
# This makes jbead accessible from Gnome Applications menu
echo -e "[Desktop Entry]\nVersion=1.0\nName=jbead\nComment=Design bead rope crochet\nExec=/opt/jbead/jbead\nIcon=/opt/jbead/jbead-32.png\nTerminal=false\nType=Application\nStartupNotify=false\n\nX-Desktop-File-Install-Version=0.23" | sudo tee /usr/share/applications/jbead.desktop &>/dev/null&