#!/bin/bash

# Android SDK Tools installation

# author:  Tukusej's Sirs
# date:    19 February 2020
# version: 1.0


# Variables
TEMP='/dev/shm'

# Get the URL
URL="$(curl -s https://developer.android.com/studio/index.html | grep -o 'https[^"]*sdk-tools-linux.*zip')"

# Download the archive
curl -sLo "${TEMP}/sdk-tools-linux.zip" "$URL"

# Create installation folder
mkdir -p /opt/android_sdk_tools

# Remove the old installation
rm -rf /opt/android_sdk_tools/*

# Decompress the archive
# unzip "${TEMP}/platform-tools-latest-linux.zip" -d /opt
unzip -qq "${TEMP}/sdk-tools-linux.zip" -d /opt/android_sdk_tools

# Install the latest version of the platforms tools
# Note: For options, see https://developer.android.com/studio/command-line/sdkmanager
echo y | /opt/android_sdk_tools/tools/bin/sdkmanager platform-tools --channel=0 &>/dev/null

# Create symblinks of all executables to /opt/bin
BIN_SDK="$(ls -l /opt/android_sdk_tools/tools | grep '^[^d][^ ]*x' | grep -o '[^ ]*$' | tr '\n' ' ')"
BIN_SDK_BIN="$(ls -l /opt/android_sdk_tools/tools/bin | grep '^[^d][^ ]*x' | grep -o '[^ ]*$' | tr '\n' ' ')"
BIN_PT="$(ls -l /opt/android_sdk_tools/platform-tools | grep '^[^d][^ ]*x' | grep -o '[^ ]*$' | sed '/\.conf$\|sqlite3/d' | tr '\n' ' ')"

for file in $BIN_SDK; do
	rm -rf /opt/bin/$file
	ln -s /opt/android_sdk_tools/tools/$file /opt/bin/$file
done

for file in $BIN_SDK_BIN; do
	rm -rf /opt/bin/$file
	ln -s /opt/android_sdk_tools/tools/bin/$file /opt/bin/$file
done

for file in $BIN_PT; do
	rm -rf /opt/bin/$file
	ln -s /opt/android_sdk_tools/platform-tools/$file /opt/bin/$file
done

rm -rf "${TEMP}/sdk-tools-linux.zip"  # "${TEMP}/licenses"