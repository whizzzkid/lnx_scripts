#!/bin/bash

# Install MTConnect Agent (c++)

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


# Install the dependencies
# Note: that gcc must have C++11
yum -y install libxml2 libxml2-devel cmake3 git cppunit-devel gcc @development

# Clone the repo
mkdir -p /git/mtconnect_agent
git clone https://github.com/mtconnect/cppagent.git /git/mtconnect_agent

# Build and install the agent
cd /git/mtconnect_agent
git submodule init
git submodule update
mkdir build && cd build
cmake3 --config Release ..
cmake3 --build . --config Release --target agent
sudo make install

# Create a user for the mtconnect service to run under
sudo useradd -r -s /bin/false mtconnect

# Make a logging directory
sudo mkdir /var/log/mtconnect
sudo chown mtconnect:mtconnect /var/log/mtconnect

# Create a directory for the agent
sudo mkdir -p /etc/mtconnect/cfg
sudo cp ../unix/agent.cfg /etc/mtconnect/cfg/
sudo cp agent/agent /usr/local/bin/
sudo cp -r ../styles ../schemas ../simulator /etc/mtconnect/
sudo chown -R mtconnect:mtconnect /etc/mtconnect

# Install the service for systemd
sudo cp ../unix/mtcagent.service ../unix/mtcsimulator.service /etc/systemd/system/
sudo systemctl enable mtcagent
sudo systemctl start mtcagent
sudo systemctl enable mtcsimulator
sudo systemctl start mtcsimulator