#!/bin/bash

# This script installs xclip from source

# author:  Tukusej's Sirs
# date:    2 November 2019
# version: 1.0


sudo dnf -y install libXmu
old_pwd="$PWD"
mkdir -p "${XDG_GIT_DIR}/others/xclip"

if [ $(ssh -T git@gitlab.com) = 'Permission denied (publickey).' ]; then
	url='https://github.com/astrand/xclip.git'
else
	url='git@github.com:astrand/xclip.git'
fi

git clone $url "${XDG_GIT_DIR}/others/xclip"
cd "${XDG_GIT_DIR}/others/xclip"
autoreconf
./configure
cd "$old_pwd"