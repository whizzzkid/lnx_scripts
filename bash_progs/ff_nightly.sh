#!/bin/bash

# This script installs Firefox Nightly

# author:  Tukusej's Sirs
# date:    24 January 2020
# version: 1.4


temp="/dev/shm"
bash_shebang='#!/bin/bash'

mkdir -p ${temp}
sudo rm -rf /opt/{ff_nightly,firefox}
curl -o ${temp}/ff_nightly.tar.bz2 -sL "https://download.mozilla.org/?product=firefox-nightly-latest-l10n-ssl&os=linux64&lang=en-GB"
sudo tar xf "${temp}/ff_nightly.tar.bz2" -C "/opt"
sudo mv /opt/firefox /opt/ff_nightly
rm ${temp}/ff_nightly.tar.bz2

# Create .desktop file
# This makes Firefox Nightly accessible from Gnome Applications menu
echo -e "[Desktop Entry]\nVersion=1.0\nName=Firefox Nightly\nGenericName=Web Browser\nComment=Browse the Web\nExec=/opt/ff_nightly/firefox %u\nIcon=/opt/ff_nightly/browser/chrome/icons/default/default128.png\nTerminal=false\nType=Application\nMimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;\nStartupNotify=false\nCategories=Network;WebBrowser;\nKeywords=web;browser;internet;\nActions=new-window;new-private-window;\n\nX-Desktop-File-Install-Version=0.23\n\n[Desktop Action new-window]\nName=New Window\nExec=/opt/ff_nightly/firefox %u\n\n[Desktop Action new-private-window]\nName=New Private Window\nExec=/opt/ff_nightly/firefox --private-window %u" | sudo tee /usr/share/applications/firefox_nightly.desktop &>/dev/null

# Create a script to run FF Nightly in `/opt/bin`
# Note: The `TZ=UTC` sets the timezone to forbid potential browser fingerprinting (src: https://wiki.archlinux.org/index.php/Firefox/Privacy#Change_browser_time_zone)
echo -e "$bash_shebang\nTZ=UTC /opt/ff_nightly/firefox \$@" | sudo tee /opt/bin/ff_nightly >/dev/null
sudo chmod a+x /opt/bin/ff_nightly
sudo chown root:root /opt/bin/ff_nightly