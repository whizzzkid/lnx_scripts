#!/bin/bash

# Installation of Shrew

# Not working yet on CentOS 8
# On CentOS 7, this works, but it is not tidied up

# author:  Tukusej's Sirs
# date:    31 December 2019
# version: 0.1


path="${HOME}/shrew"
mkdir -p "$path"
sudo dnf -y install gcc-c++ openssl-devel cmake cmake3 libedit-devel checkinstall flex bison qwt5-qt4* qt-devel qt-config qt5-qtbase
# not found: checkinstall gwt5-qt4
# installed: libedit-devel qt
wget --secure-protocol=TLSv1 -O "$path/ike-2.2.1-release.tbz2" https://www.shrew.net/download/ike/ike-2.2.1-release.tbz2
tar xf "$path/ike-2.2.1-release.tbz2" -C "$path"
cd "$path/ike"



cmake -DCMAKE_INSTALL_PREFIX=/usr -DQTGUI=YES -DETCDIR=/etc -DNATT=YES .
make && sudo make install && rm -rf "$path"
sudo mv /etc/iked.conf{.sample,}
sudo su -c "echo 'include /usr/lib' >> /etc/ld.so.conf"
sudo ldconfig
sudo su -c "echo '/usr/lib' > /etc/ld.so.conf"  # Note: There must be no trailing slash in `/usr/lib`, or it will fail

# Create service for `iked` daemon
echo -e '[Unit]\nDescription=iked\nAfter=syslog.target network.target\n[Service]\nType=simple\nUser=root\nExecStart=/usr/sbin/iked -F\nRestart=on-abort\n[Install]\nWantedBy=multi-user.target' | sudo tee /etc/systemd/system/iked.service &>/dev/null
sudo systemctl daemon-reload
sudo systemctl enable iked
sudo systemctl start iked