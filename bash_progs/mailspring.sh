#!/bin/bash

# This script installs MailSpring

# author:  Tukusej's Sirs
# date:    26 December 2019
# version: 1.1


temp="/dev/shm"
sudo dnf -y install redhat-lsb-core libXScrnSaver
wget -O ${temp}/mailspring.rpm https://updates.getmailspring.com/download?platform=linuxRpm
sudo dnf -y install ${temp}/mailspring.rpm
# CentOS 8: If nothing provides `libappindicator`, you can force install is using the following command
# sudo rpm -i --nodeps  ${temp}/mailspring.rpm
rm ${temp}/mailspring.rpm