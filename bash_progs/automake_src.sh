# This script installs `automake` from source

# author:  Tukusej's Sirs
# date:    2 September 2019
# version: 1.1


sudo rpm -e --nodeps automake
sudo yum -y install autogen
mkdir -p ${XDG_GIT_DIR}/automake
git clone https://git.savannah.gnu.org/git/automake.git ${XDG_GIT_DIR}/automake
cd ${XDG_GIT_DIR}/automake
# aclocal
# autoconf
# chmod a+x configure
./bootstrap
./configure
make && sudo make install
sudo ln -s /usr/share/aclocal/pkg.m4 /usr/local/share/aclocal/pkg.m4