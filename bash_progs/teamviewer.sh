#!/bin/bash

# This script installs TeamViewer

# author:  Tukusej's Sirs
# date:    3 September 2019
# version: 1.1


temp="/dev/shm"
sudo rpm --import https://dl.tvcdn.de/download/linux/signature/TeamViewer2017.asc
sudo yum -y install epel-release
wget -O ${temp}/teamviewer.x86_64.rpm https://download.teamviewer.com/download/linux/teamviewer.x86_64.rpm
sudo yum -y install ${temp}/teamviewer.x86_64.rpm
rm ${temp}/teamviewer.x86_64.rpm
