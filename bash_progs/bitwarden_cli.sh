#!/bin/bash

# This script installs Bitwarden CLI

# author:  Tukusej's Sirs
# date:    19 January 2020
# version: 1.6

# TODO
# - check if `bw` alias is defined in .bashrc (and all the source files) and only add it if it is not in it


# Password
if [ "$1" != '' ]; then
	password="$1"
	sudo_cmd="echo \"$password\" | sudo -Sp ''"

	function sudo_cmd() {
		args=$@

		if [ "$1" = 'bash' ]; then
			args=$(echo "$args" | sed 's/^bash -c //')
			echo "$password" | sudo -Sp '' bash -c "$args"
		else
			echo "$password" | sudo -Sp '' "$@"
		fi
	}
else
	function sudo_cmd() {
		sudo $@
	}
fi

# Set `sudo` timeout to zero
echo "Defaults:ts timestamp_timeout=0" | sudo_cmd tee -a /etc/sudoers &>/dev/null

# Install dependencies
sudo_cmd yum -y install npm jq

# CLI app
old_pwd="$PWD"
mkdir -p "/git/others/bw"

# Get Git URL (SSH or HTTPS)
gitlab_host_key='gitlab.com,35.231.145.151 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY='
if [ -f "${HOME}/.ssh/known_hosts" ] && [ "$(grep "$gitlab_host_key" ${HOME}/.ssh/known_hosts)" = "$gitlab_host_key" ]; then
	if [ "$(ssh -T git@gitlab.com 2>&1 | grep 'Permission denied (publickey).')" = 'Permission denied (publickey).' ]; then
		url='https://github.com/bitwarden/cli.git'
	else
		url='git@github.com:bitwarden/cli.git'
	fi
else
	echo "$gitlab_host_key" >> ${HOME}/.ssh/known_hosts
	url='https://github.com/bitwarden/cli.git'
fi

git clone $url "/git/others/bw"
cd "/git/others/bw"

npm install
npm run sub:init       # To initialise the Git submodule for `jslib`

# Create and enable a system service that would run the bw_agent in the background
sudo_cmd bash -c "echo -e \"[Unit]\nDescription=bw_agent\nAfter=syslog.target network.target\n[Service]\nType=simple\nUser=$USER\nExecStart=$(which npm) run --prefix \\\"/git/others/bw\\\" build:watch\nRestart=on-abort\n[Install]\nWantedBy=multi-user.target\" > /etc/systemd/system/bw_agent.service"
sudo_cmd systemctl daemon-reload
sudo_cmd systemctl enable bw_agent
sudo_cmd systemctl start bw_agent

# Enable running `bw_agent` service using `semodule`
sudo_cmd ausearch -c 'npm' --raw | audit2allow -M my-npm && sudo_cmd semodule -X 300 -i my-npm.pp && rm my-npm.*

# Create an alias
# TODO
# alias bw="node \"/git/others/bw/build/bw.js\""

# Change to the $old_pwd
cd "$old_pwd"

# Set `sudo` timeout back to the original
sudo sed -i "/Defaults:$USER timestamp_timeout=0/d" /etc/sudoers