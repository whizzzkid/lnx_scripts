#!/bin/bash

# This script builds and installs Translate Shell from source

# author:  Tukusej's Sirs
# date:    7 January 2020
# version: 1.0


# Install build and run-time dependencies (listing all except GNU Bash / Zsh and GNU Awk; when multiple options are available, I have chosen one)
sudo dnf -y install curl fribidi espeak less aspell

# Clone the repo
mkdir -p "${XDG_GIT_DIR}/others/trans"
git clone git@github.com:soimort/translate-shell.git "${XDG_GIT_DIR}/others/trans"
cd "${XDG_GIT_DIR}/others/trans"

# Build and install
make && sudo make install