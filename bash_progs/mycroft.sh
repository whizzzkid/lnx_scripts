#!/bin/bash

# This function installs Mycroft AI, an open-source personal assistant

# author:  Tukusej's Sirs
# date:    3 September 2019
# version: 0.1


mkdir -p "$XDG_GIT_DIR/mycroft"
git clone git@github.com:MycroftAI/mycroft-core.git "$XDG_GIT_DIR/mycroft"
cd "$XDG_GIT_DIR/mycroft"
echo -e "Y\nY\nY\nY\nY\n" > bash dev_setup.sh