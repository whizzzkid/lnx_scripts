#!/bin/bash

# This script installs youtube-dl from source on RedHat-like systems

# author:  Tukusej's Sirs
# date:    10 November 2019
# version: 1.0


# Install dev dependencies
sudo yum -y install python make pandoc zip nosetests

# Clone the repo
mkdir -p ${XDG_GIT_DIR}/others/youtube-dl
git clone git@github.com:ytdl-org/youtube-dl.git ${XDG_GIT_DIR}/others/youtube-dl
cd ${XDG_GIT_DIR}/others/youtube-dl

# Build and install
make && sudo make install