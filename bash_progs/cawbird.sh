#!/bin/bash

# This script installs Cawbird

# author:  Tukusej's Sirs
# date:    3 February 2020
# version: 1.0


# One could use this to install the official package from official repository
# sudo yum -y install yum-utils
# sudo yum-config-manager --add-repo https://download.opensuse.org/repositories/home:IBBoard:cawbird/CentOS_$(rpm -$ %centos)/home:IBBoard:cawbird.repo
# sudo yum -y install cawbird


# Build from source
# PowerTools: ninja-build meson vala gspell-devel
# rpmfusion-free-updates: gstreamer1-libav
sudo dnf config-manager --set-enabled PowerTools
sudo dnf -y install http://download1.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %centos).noarch.rpm
sudo dnf -y install cmake3 gtk3 glib2 json-glib sqlite libsoup gettext vala meson ninja-build gstreamer1-plugins-base gstreamer1-plugins-base-devel gstreamer1-plugins-bad-free gstreamer1-libav gspell gspell-devel json-glib-devel sqlite-devel libsoup-devel

git clone git@github.com:IBBoard/cawbird.git "${XDG_GIT_DIR}/others/cawbird"
cd "${XDG_GIT_DIR}/others/cawbird"
# meson --prefix /opt build
meson build
cd build
ninja-build
# sudo DESTDIR=/opt/cawbird ninja-build install
sudo ninja-build install