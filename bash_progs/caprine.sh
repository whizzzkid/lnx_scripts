#!/bin/bash

# This script installs latest version of Caprine app (desktop app for Facebook)

# author:  Tukusej's Sirs
# date:    25 January 2020
# version: 1.2

# Exit codes:
# 1        Could not get latest version number
# 2        Could not get latest version of AppImage


# User-dependent variables
path='/opt/caprine'
shebang='#!/bin/bash'
mkdir -p "$path"
rm -rf $path/*

version="$(curl -s "https://api.github.com/repos/sindresorhus/caprine/releases/latest" | grep -Po "^[\t ]*\"tag_name\": \"v\K[^\"]*")"
if [ ! "$version" ]; then
	echo "I could not find the latest version number. Check it at https://github.com/sindresorhus/caprine/releases/latest. And then it would be nice of you if you report this issue at https:/gitlab.com/tukusejssirs/lnx_scripts/issues (make sure there is no such report yet please)."
	exit 1
fi

wget -qO "$path/caprine_${version}.appimage" "https://github.com/sindresorhus/caprine/releases/download/v${version}/Caprine-${version}.AppImage"
test_1="$?"

if [ $test_1 != 0 ]; then
	wget -qO "$path/caprine_${version}.appimage" "https://github.com/sindresorhus/caprine/releases/download/v${version}/caprine-${version}-x86_64.AppImage"
	test_2="$?"

	if [ $test_2 != 0 ]; then
		echo "I could not find the latest version of Caprine AppImage. Check it at https://github.com/sindresorhus/caprine/releases/latest. And then it would be nice of you if you report this issue at https:/gitlab.com/tukusejssirs/lnx_scripts/issues (make sure there is no such report yet please)."
		exit 2
	fi
fi

chmod a+x "$path/caprine_${version}.appimage"

# Create a winbox executable
echo -e "${shebang}\n${path}/caprine_${version}.appimage &>/dev/null &" | sudo tee /opt/bin/caprine >/dev/null
sudo chmod a+x /opt/bin/caprine

# Download icon image
curl -so /opt/caprine/caprine.png https://raw.githubusercontent.com/sindresorhus/caprine/master/static/Icon.png

# Create .desktop file
# This makes Caprine accessible from Gnome Applications menu
echo -e "[Desktop Entry]\nVersion=1.0\nName=Caprine\nComment=Facebook Messenger\nExec=/opt/bin/caprine\nIcon=/opt/caprine/caprine.png\nTerminal=false\nType=Application\nStartupNotify=false\n# Categories=;\n# Keywords=;\n\nX-Desktop-File-Install-Version=0.23" | sudo tee /usr/share/applications/caprine.desktop &>/dev/null

# Fix file ownership
sudo chown -R root:root /opt/caprine /opt/bin/caprine