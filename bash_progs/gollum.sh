#!/bin/bash

# This script installs Gollum

# author:  Tukusej's Sirs
# date:    29 August 2019
# version: 1.0


# Install Gollum
sudo yum -y group install "Development Tools"
sudo yum -y install libicu libicu-devel
sudo gem install nokogiri gollum github-markdown