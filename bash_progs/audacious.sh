#!/bin/bash

# This script builds latest dev version of Audacious from source

# author:  Tukusej's Sirs
# date:    18 February 2020
# version: 1.1


# Enable PowerTools repo
# TODO: check if it is not enabled; if yes, enable it
sudo dnf -y config-manager --set-enabled PowerTools

# Update the database for mlocate
# Note: It happened on freshly installed CentOS Stream 8 that the database was not updated and the new shared libraries were not available
sudo ionice -c3 updatedb

# Make the LDD read the shared libraries in /usr/local/lib
echo "/opt/lib" | sudo tee /etc/ld.so.conf.d/usr_local_lib.conf > /dev/null
sudo ldconfig

# Install dependencies for audacious and audacious-plugins
# not found: build-essential libasound2-dev libavformat-dev libcddb2-dev libcdio-cdda-dev libcurl4-gnutls-dev libgl1-mesa-dev-lts-utopic libgtk2.0-dev libjack-jackd2-dev liblircclient-dev libmp3lame-dev libneon27-gnutls-dev libpulse-dev libsamplerate0-dev libsdl1.2-dev libsndfile1-dev libvorbis-dev libwavpack-dev
sudo dnf -y install glib2-devel	dbus-glib-devel automake git libbinio-devel libbs2b-devel libcue-devel flac-libs fluidsynth-devel fluidsynth-libs fluidsynth libguess-devel libmms-devel libmodplug-devel mpg123-libs libnotify-devel libsamplerate SDL-devel libsidplayfp-devel libsndfile soxr-devel libvorbis wavpack libxml2-devel adplug flac flac-devel libvorbis-devel mpg123-devel wavpack-devel neon-devel ffmpeg-devel

# Install audacious from src
if [ $(ssh -T git@gitlab.com) = 'Permission denied (publickey).' ]; then
	url='https://github.com/audacious-media-player/audacious.git'
else
	url='git@github.com:audacious-media-player/audacious.git'
fi

git clone $url "${XDG_GIT_DIR}/others/audacious"
old_path="$PWD"
cd "${XDG_GIT_DIR}/others/audacious"
./autogen.sh
./configure --prefix=/opt
make && sudo make install

# Update PKG_CONFIG_PATH and LD_LIBRARY_PATH (otherwise `./configure` in audacious-plugins would not find `audacious`)
if [ "$PKG_CONFIG_PATH" = '' ]; then
	export PKG_CONFIG_PATH=/opt/lib/pkgconfig
else
	export PKG_CONFIG_PATH=/opt/lib/pkgconfig:$PKG_CONFIG_PATH
fi

if [ "$PKG_CONFIG_PATH" = '' ]; then
	LD_LIBRARY_PATH=/opt/lib
else
	LD_LIBRARY_PATH=/opt/lib:$LD_LIBRARY_PATH
fi

echo "/opt/lib" | sudo tee /etc/ld.so.conf.d/usr_local_lib.conf > /dev/null
sudo ldconfig

# Install audacious-plugins from src
git clone git@github.com:audacious-media-player/audacious-plugins.git "${XDG_GIT_DIR}/others/audacious-plugins"
cd "${XDG_GIT_DIR}/others/audacious-plugins"
./autogen.sh
./configure --prefix=/opt
make && sudo make install
cd "$old_path"