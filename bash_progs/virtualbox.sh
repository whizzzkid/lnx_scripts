#!/bin/bash

# This script installs VirtualBox with Guest Additions extension pack on CentOS 8

# author:  Tukusej's Sirs
# date:    20 January 2020
# version: 1.0

# TODO
# - test on vanilla CentOS 8


temp="/dev/shm"

# Add repos
sudo dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %centos).noarch.rpm
sudo dnf config-manager --add-repo=https://download.virtualbox.org/virtualbox/rpm/el/virtualbox.repo
sudo rpm --import https://www.virtualbox.org/download/oracle_vbox.asc

# Install latest VirtualBox version
vb_latest="$(dnf search VirtualBox 2>/dev/null | tail -1 | grep -oP '^VirtualBox-[0-9.]*(?=\.x86_64)')"
sudo dnf -y install $vb_latest

# Install latest Oracle VM VirtualBox Extension Pack
ext_latest=$(curl -s https://download.virtualbox.org/virtualbox/LATEST.TXT)
wget -O ${temp}/Oracle_VM_VirtualBox_Extension_Pack-${ext_latest}.vbox-extpack / https://download.virtualbox.org/virtualbox/${ext_latest}/Oracle_VM_VirtualBox_Extension_Pack-${ext_latest}.vbox-extpack
echo "y" | sudo VBoxManage extpack install --replace ${temp}/Oracle_VM_VirtualBox_Extension_Pack-${ext_latest}.vbox-extpack
rm ${temp}/Oracle_VM_VirtualBox_Extension_Pack-${ext_latest}.vbox-extpack
sudo akmods
sudo systemctl restart systemd-modules-load.service