#!/bin/bash

# Install Sublime Text 3 and crack it

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


# Add repos
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

# Install Sublime Merge 3
sudo dnf -y install sublime-text

# Crack Sublime Text 3
subl_ver=$(subl --version | grep -o '[0-9]*$')
url="https://cynic.al/warez/$(curl -s https://cynic.al/warez/ | grep -o "sublime-text/${subl_ver}-linux/sublime_text_linux${subl_ver}cracked")"
mv /opt/sublime_text/sublime_text{,.bak}
curl -so /opt/sublime_text/sublime_text "$url"
# It might be required to 're-crack' it after upgrade
cp /opt/sublime_text/sublime_text /opt/sublime_text/sublime_text.mdfd
# Install the dummy licence
curl -so ~/.config/sublime-text/Local/License.sublime_license https://cynic.al/warez/sublime-text/st_dummy_licence.txt