#!/bin/bash

# This script installs GeoGebra 6 on RedHat-like systems

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.1

# $1       version to install [5 or 6; default is 6]


# Variables
ver=$1
temp="/dev/shm"

# Update packages
sudo dnf -y upgrade

if [ $ver ] && [ $ver = 5 ]; then
	# GeoGebra 5
	curl -so ${temp}/geogebra.rpm "https://www.geogebra.org/download/rpm.php?arch=amd64"
	sudo dnf -y install ${temp}/geogebra.rpm  # There might be required other dependencies, just like with v6
else
	# GeoGebra Classic 6
	curl -so ${temp}/geogebra.rpm "https://www.geogebra.org/download/rpm.php?arch=amd64&ver=6"
	sudo dnf -y install gtk2.i686 libX11-xcb.i686 libXtst.i686 GConf2.i686 nss.i686 alsa-lib.i686 ${temp}/geogebra.rpm
fi

# Clean up
rm ${temp}/geogebra.rpm