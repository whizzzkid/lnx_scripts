#!/bin/bash

# This script installs TexLive 2019

# author:  Tukusej's Sirs
# date:    14 December 2019
# version: 1.2


temp="/dev/shm/texlive"
mkdir -p "$temp"
curl -so "$temp/ctan-mirrors.pl" "https://comedy.dante.de/~erik/ctan-mirrors/ctan-mirrors"
chmod a+x "$temp/ctan-mirrors.pl"

# Install dependencies
sudo dnf -y install perl perl-experimental perl-App-cpanminus fping
cpanm List::MoreUtils LWP::Protocol::https

# Get the best CTAN mirror
ctan_mirror=$(perl "$temp/ctan-mirrors.pl" -m 1 -p rsync 2>/dev/null | grep -Po "^.*\Krsync.*$")
rm -rf "$temp/FileFetch*" "$temp/ctan-mirrors.pl"

rsync -a --delete ${ctan_mirror}/systems/texlive/tlnet/ "$temp/texlive" && cd "$temp/texlive" && echo i | sudo ./install-tl

# Add TexLive 2019 to PATH
if [ -e /usr/local/texlive/2019/bin/x86_64-linux ]; then
	export PATH="$PATH:/usr/local/texlive/2019/bin/x86_64-linux"
fi

# Add TexLive 2019 to MANPATH.
if [ -e /usr/local/texlive/2019/texmf-dist/doc/man ]; then
	export MANPATH="/usr/local/texlive/2019/texmf-dist/doc/man"
fi

# Add TexLive 2019 to INFOPATH.
if [ -e /usr/local/texlive/2019/texmf-dist/doc/info ]; then
	export INFOPATH="/usr/local/texlive/2019/texmf-dist/doc/info"
fi

rm -rf "$temp"