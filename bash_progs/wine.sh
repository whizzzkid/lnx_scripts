#!/bin/bash

# Download, build and install latest 32-bit and 64-bit wine on CentOS 7

# For details of this script, please check
# https://www.systutorials.com/239913/install-32-bit-wine-1-8-centos-7/
# src: https://github.com/zma/usefulscripts/blob/master/script/install-wine-i686-centos7.sh

# author:  Eric Zhiqiang Ma (zma@ericzma.com); modified by Tukusej's Sirs
# date:    30 August 2019
# version: 1.2

# TODO
# - remove verbous comments (make them short)
# - remove $1 check (always install the latest version)
# - install Nux Desktop repo on Centos; check if it is required on Fedora


os=$(grep -Po "^ID=\K.*$" /etc/os-release)
### CENTOS 7
case "$os" in
	centos)
		# Installation
		ver_folder=$(curl "http://dl.winehq.org/wine/source/?C=M;O=D" 2>/dev/null | html2text | grep -C2 Parent | grep -Po '/share/theme/icons/folder.png\)]\(\K[0-9x.]*')
		ver=$(curl "http://dl.winehq.org/wine/source/${ver_folder}/?C=M;O=D" 2>/dev/null | html2text | grep -C2 Parent | grep -Po "wine-\K[0-9.]*(?=.tar)")

		echo "Hello there. Start to download, build and install wine $ver 32-bit version..."

		echo "Please make sure you have EPEL and Nux Desktop repositories configured. Check https://www.systutorials.com/239893/additional-repositories-centos-linux/ for howto."
		echo "Uninstall old wine64 if you have installed it. Please select yes..."

		yum -y remove wine wine-*  # TODO: some problem when no such pkg is installed

		echo "Install wine building tools..."

		yum -y groupinstall 'Development Tools'  # TODO: some problem here
		yum -y install samba-winbind-clients libjpeg-turbo-devel libtiff-devel freetype-devel glibc-devel.{i686,x86_64} gcc.i386 libgcc.{i686,x86_64} libX11-devel.{i686,x86_64} freetype-devel.{i686,x86_64} gnutls-devel.{i686,x86_64} libxml2-devel.{i686,x86_64} libjpeg-turbo-devel.{i686,x86_64} libpng-devel.{i686,x86_64} libXrender-devel.{i686,x86_64} alsa-lib-devel.{i686,x86_64} glib2-devel.{i686,x86_64} libSM-devel.{i686,x86_64} glibc-devel libstdc++-devel icoutils openal-soft-devel prelink gstreamer-plugins-base-devel gstreamer-devel ImageMagick-devel fontpackages-devel libv4l-devel gsm-devel giflib-devel libXxf86dga-devel mesa-libOSMesa-devel isdn4k-utils-devel libgphoto2-devel fontforge libusb-devel lcms2-devel audiofile-devel openldap-devel libxslt-devel libXcursor-devel libXi-devel libXxf86vm-devel libXrandr-devel libXinerama-devel libXcomposite-devel mesa-libGLU-devel ocl-icd opencl-headers libpcap-devel dbus-devel ncurses-devel libsane-hpaio pulseaudio-libs-devel cups-devel libmpg123-devel fontconfig-devel sane-backends-devel.x86_64 glibc-devel.i686 dbus-devel.i686 freetype-devel.i686 pulseaudio-libs-devel.i686 libX11-devel.i686 mesa-libGLU-devel.i686 libICE-devel.i686 libXext-devel.i686 libXcursor-devel.i686 libXi-devel.i686 libXxf86vm-devel.i686 libXrender-devel.i686 libXinerama-devel.i686 libXcomposite-devel.i686 libXrandr-devel.i686 mesa-libGL-devel.i686 mesa-libOSMesa-devel.i686 libxml2-devel.i686 libxslt-devel.i686 zlib-devel.i686 gnutls-devel.i686 ncurses-devel.i686 sane-backends-devel.i686 libv4l-devel.i686 libgphoto2-devel.i686 libexif-devel.i686 lcms2-devel.i686 gettext-devel.i686 isdn4k-utils-devel.i686 cups-devel.i686 fontconfig-devel.i686 gsm-devel.i686 libjpeg-turbo-devel.i686 pkgconfig.i686 libtiff-devel.i686 unixODBC.i686 openldap-devel.i686 alsa-lib-devel.i686 audiofile-devel.i686 freeglut-devel.i686 giflib-devel.i686 gstreamer-devel.i686 gstreamer-plugins-base-devel.i686 libXmu-devel.i686 libXxf86dga-devel.i686 libieee1284-devel.i686 libpng-devel.i686 librsvg2-devel.i686 libstdc++-devel.i686 libusb-devel.i686 unixODBC-devel.i686 qt-devel.i686 libpcap-devel.i686 gstreamer1-plugins-base-devel.{x86_64,i686} gstreamer1-devel.{x86_64,i686} systemd-devel.{x86_64,i686} libXfixes-devel.{x86_64,i686} winetools

		echo "Download and unpack the wine source package..."

		cd /usr/src
		wget -O wine-${ver}.tar.xz http://dl.winehq.org/wine/source/${ver_folder}/wine-${ver}.tar.xz
		tar xf wine-${ver}.tar.xz

		echo "Build wine..."
		cd wine-${ver}/
		mkdir -p wine32 wine64

		echo "   build wine64..."
		cd wine64
		../configure --enable-win64
		make -j 4

		echo "   build wine32..."
		cd ../wine32
		PKG_CONFIG_PATH=/usr/lib/pkgconfig ../configure --with-wine64=../wine64
		make -j 4

		echo "Install wine..."
		echo "   install wine32..."
		make install

		echo "   install wine64..."
		cd ../wine64
		make install

		echo "Congratulation! All are done. Enjoy!"

		# # Uninstall
		# cd /usr/src/wine-${ver}/wine32
		# make uninstall
		# cd /usr/src/wine-${ver}/wine64
		# make uninstall
	;;
	### FEDORA 30
	fedora)
#		sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/30/winehq.repo
#		sudo dnf -y install winehq-stable.{x86_64,i686}

		sudo dnf -y install alsa-plugins-pulseaudio.i686 glibc-devel.i686 glibc-devel libgcc.i686 libX11-devel.i686 freetype-devel.i686 libXcursor-devel.i686 libXi-devel.i686 libXext-devel.i686 libXxf86vm-devel.i686 libXrandr-devel.i686 libXinerama-devel.i686 mesa-libGLU-devel.i686 mesa-libOSMesa-devel.i686 libXrender-devel.i686 libpcap-devel.i686 ncurses-devel.i686 libzip-devel.i686 lcms2-devel.i686 zlib-devel.i686 libv4l-devel.i686 libgphoto2-devel.i686 cups-devel.i686 libxml2-devel.i686 openldap-devel.i686 libxslt-devel.i686 gnutls-devel.i686 libpng-devel.i686 flac-libs.i686 json-c.i686 libICE.i686 libSM.i686 libXtst.i686 libasyncns.i686 libedit.i686 liberation-narrow-fonts.noarch libieee1284.i686 libogg.i686 libsndfile.i686 libuuid.i686 libva.i686 libvorbis.i686 libwayland-client.i686 libwayland-server.i686 llvm-libs.i686 mesa-dri-drivers.i686 mesa-filesystem.i686 mesa-libEGL.i686 mesa-libgbm.i686 nss-mdns.i686 ocl-icd.i686 pulseaudio-libs.i686 sane-backends-libs.i686 tcp_wrappers-libs.i686 unixODBC.i686 samba-common-tools.x86_64 samba-libs.x86_64 samba-winbind.x86_64 samba-winbind-clients.x86_64 samba-winbind-modules.x86_64 mesa-libGL-devel.i686 fontconfig-devel.i686 libXcomposite-devel.i686 libtiff-devel.i686 openal-soft-devel.i686 mesa-libOpenCL-devel.i686 opencl-utils-devel.i686 alsa-lib-devel.i686 gsm-devel.i686 libjpeg-turbo-devel.i686 pulseaudio-libs-devel.i686 pulseaudio-libs-devel gtk3-devel.i686 libattr-devel.i686 libva-devel.i686 libexif-devel.i686 libexif.i686 glib2-devel.i686 mpg123-devel.i686 mpg123-devel.x86_64 libcom_err-devel.i686 libcom_err-devel.x86_64 libFAudio-devel.i686 libFAudio-devel.x86_64 gstreamer-plugins-base-devel gstreamer-devel.i686 gstreamer.i686 gstreamer-plugins-base.i686 gstreamer-devel gstreamer1.i686 gstreamer1-devel gstreamer1-plugins-base-devel.i686 gstreamer-plugins-base.x86_64 gstreamer.x86_64 gstreamer1-devel.i686 gstreamer1-plugins-base-devel gstreamer-plugins-base-devel.i686 gstreamer-ffmpeg.i686 gstreamer1-plugins-bad-free-devel.i686 gstreamer1-plugins-bad-free-extras.i686 gstreamer1-plugins-good-extras.i686 gstreamer1-libav.i686 gstreamer1-plugins-bad-freeworld.i686

		sudo ln -s /usr/include/freetype2 /usr/include/freetype

# No match for argument: libNX_Xext-devel.i686
# No match for argument: libcapifax-devel.i686
# No match for argument: python-talloc.x86_64



		sudo dnf -y group install "C Development Tools and Libraries" "Development Tools"
		mkdir -p ${HOME}/git/wine/
		git clone git://source.winehq.org/git/wine.git ${HOME}/git/wine
		mkdir ${HOME}/git/wine/wine{32,64}

		# Compile wine64
		cd ${HOME}/git/wine/wine64
		../configure --enable-win64
		make -j 4

		# Compile wine32
		cd ${HOME}/git/wine/wine32
		PKG_CONFIG_PATH=/usr/lib/pkgconfig CC="ccache gcc -m32" ../configure --with-wine64=../wine64
		make -j 4

		echo "Install wine..."
		echo "   install wine32..."
		sudo make install

		echo "   install wine64..."
		cd ../wine64
		sudo make install
	;;
	*)
		echo "ERROR: This script currennntly supports Fedora 30 and CentOS 7 only."
		exit 1
esac