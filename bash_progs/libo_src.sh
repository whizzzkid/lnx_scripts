#!/bin/bash

# Build and install latest version of LibreOffice from source

# author:  Tukusej's Sirs
# date:    19 February 2020
# version: 0.2


temp='/dev/shm'

# Get latest version number
ver=$(curl -s "https://download.documentfoundation.org/libreoffice/stable/" | grep -Po 'href="\K[0-9.]*(?=/")' | sort -V | tail -1)
ver_maj_min=$(grep -o '^[0-9]*\.[0-9]*' <<< $ver)

# Generate archive filenames
libo="LibreOffice_${ver}_Linux_x86-64_rpm"
libo_help_en="LibreOffice_${ver}_Linux_x86-64_rpm_helppack_en-GB"
libo_help_sk="LibreOffice_${ver}_Linux_x86-64_rpm_helppack_sk"
libo_lang_en="LibreOffice_${ver}_Linux_x86-64_rpm_langpack_en-GB"
libo_lang_sk="LibreOffice_${ver}_Linux_x86-64_rpm_langpack_sk"
libo_lang_cs="LibreOffice_${ver}_Linux_x86-64_rpm_langpack_cs"
libo_lang_hu="LibreOffice_${ver}_Linux_x86-64_rpm_langpack_hu"
libo_lang_he="LibreOffice_${ver}_Linux_x86-64_rpm_langpack_he"
libo_lang_el="LibreOffice_${ver}_Linux_x86-64_rpm_langpack_el"

# Install Java
sudo dnf -y install java-11-openjdk

# Enter the $temp directory
cd "$temp"

for pkg in $libo $libo_help_en $libo_help_sk $libo_lang_en $libo_lang_sk $libo_lang_cs $libo_lang_hu $libo_lang_he $libo_lang_el; do
	# Download the archives
	echo -e "${lmagenta}$pkg${fdefault}"

	# The while loop is required as the DocumentFoundation.org host is not always resolvable
	while [ "$rc" != "0" ] || [ ! -f "$temp/$pkg.tar.gz" ]; do
		curl -#Lo "$temp/$pkg.tar.gz" "https://download.documentfoundation.org/libreoffice/stable/${ver}/rpm/x86_64/$pkg.tar.gz"
		rc=$?
	done

	unset rc

	# Decompress the archives
	tar xf "$temp/$pkg.tar.gz" -C "$temp"
done

# Redefine package variables using $ver_full
ver_full=$(ls -d1 /dev/shm/LibreOffice_*_Linux_x86-64_rpm | grep -Po '/dev/shm/LibreOffice_\K[0-9.]*')
libo="LibreOffice_${ver_full}_Linux_x86-64_rpm"
libo_help_en="LibreOffice_${ver_full}_Linux_x86-64_rpm_helppack_en-GB"
libo_help_sk="LibreOffice_${ver_full}_Linux_x86-64_rpm_helppack_sk"
libo_lang_en="LibreOffice_${ver_full}_Linux_x86-64_rpm_langpack_en-GB"
libo_lang_sk="LibreOffice_${ver_full}_Linux_x86-64_rpm_langpack_sk"
libo_lang_cs="LibreOffice_${ver_full}_Linux_x86-64_rpm_langpack_cs"
libo_lang_hu="LibreOffice_${ver_full}_Linux_x86-64_rpm_langpack_hu"
libo_lang_he="LibreOffice_${ver_full}_Linux_x86-64_rpm_langpack_he"
libo_lang_el="LibreOffice_${ver_full}_Linux_x86-64_rpm_langpack_el"

# Remove previous versions of LibreOffice
PKG_LIST="$(dnf info libreoffice[0-9.]* | grep -Po 'Name *: \Klibreoffice.*$' | tr '\n' ' ')"
sudo dnf -y remove $PKG_LIST

# Install the packages
for pkg in $libo $libo_help_en $libo_help_sk $libo_lang_en $libo_lang_sk $libo_lang_cs $libo_lang_hu $libo_lang_he $libo_lang_el; do
	sudo dnf -y install $temp/$pkg/RPMS/*rpm
done

# Create a symlink (without the version number)
sudo rm -rf /bin/libreoffice
sudo ln -s /bin/libreoffice{$ver_maj_min,}