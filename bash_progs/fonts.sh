#!/bin/bash

# This script installs select fonts (Ubuntu, Libertinus and Linux Libertine G). By default it installs all fonts.

# author:  Tukusej's Sirs
# date:    1 January 2020
# version: 1.5

# dependencies: git p7zip wget (p7zip requires EPEL repo)


# Default options
temp="/dev/shm"
UBUNTU=1
LIBERTINUS=1
LINLIBG=1

# Source ${HOME}/.config/user-dirs.dirs to be able to use the variables in shell
source ${HOME}/.config/user-dirs.dirs

# In case `user-dirs.dirs` does not define XDG_GIT_DIR variable
if [ ! "$XDG_GIT_DIR" ]; then
	XDG_GIT_DIR='/git'
fi

# Call getopt to validate the provided input.
OPTIONS=$(getopt -o a,u,l,ll,h --long all,ubuntu,libertinus,linlibg,help -- "$@")

# Short help message
[ $? -eq 0 ] || {
	echo "Incorrect options provided."
	return 1
}
eval set -- "$OPTIONS"
while true; do
	case "$1" in
	--all|-a)
		UBUNTU=1
		LIBERTINUS=1
		LINLIBG=1
	;;
	--ubuntu|-u)
		UBUNTU=1
		LIBERTINUS=0
		LINLIBG=0
	;;
	--libertinus|-l)
		UBUNTU=0
		LIBERTINUS=1
		LINLIBG=0
	;;
	--linlibg|-ll)
		UBUNTU=0
		LIBERTINUS=0
		LINLIBG=1
	;;
	--help|-h)
		# Show long help message
		# TODO
		echo "Usage: ${FUNCNAME[0]} [-a | --all] [-u | --ubuntu] [-l | --libertinus] [-ll | --linlibg] [-h | --help]"
		return 0
	;;
	--)
		shift
		OPT_REST="$@"
		break
	;;
	esac
	shift
done

if [ $OPT_REST ]; then
	echo "WARNING: $OPT_REST are ignored." 1>&2
fi

# Libertinus
if [ $LIBERTINUS = 1 ]; then
	echo -n 'Installing Libertinus font ... '
	mkdir -p ${XDG_GIT_DIR}/others/libertinus
	sudo mkdir -p /usr/share/fonts/truetype/libertinus
	git clone https://github.com/alif-type/libertinus.git ${XDG_GIT_DIR}/others/libertinus >/dev/null
	sudo ln -s ${XDG_GIT_DIR}/others/libertinus/*otf /usr/share/fonts/truetype/libertinus
	sudo chmod -R 755 /usr/share/fonts/truetype/libertinus
	echo 'done!'
fi

# Linux Libertine G
if [ $LINLIBG = 1 ]; then
	echo -n 'Installing Linux Libertine G font ... '
	sudo mkdir -p /usr/share/fonts/truetype/linlibg
	wget -qO ${temp}/linlibg.zip http://www.numbertext.org/linux/e7a384790b13c29113e22e596ade9687-LinLibertineG-20120116.zip
	sudo 7za e -i\!LinLibertineG/*.ttf -o/usr/share/fonts/truetype/linlibg ${temp}/linlibg.zip >/dev/null
	rm ${temp}/linlibg.zip
	sudo chmod -R 755 /usr/share/fonts/truetype/linlibg
	echo 'done!'
fi

# Ubuntu
if [ $UBUNTU = 1 ]; then
	echo -n 'Installing Ubuntu font ... '
	sudo mkdir -p /usr/share/fonts/truetype/ubuntu
	wget -O ${temp}/ubuntu.zip https://assets.ubuntu.com/v1/0cef8205-ubuntu-font-family-0.83.zip
	sudo 7za e -i\!ubuntu-font-family-0.83/*.ttf -o/usr/share/fonts/truetype/ubuntu ${temp}/ubuntu.zip
	rm ${temp}/ubuntu.zip
	sudo chmod -R 755 /usr/share/fonts/truetype/ubuntu
	echo 'done!'
fi

# Force re-build font information cache files
sudo fc-cache -f