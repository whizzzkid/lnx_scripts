#!/bin/bash

# This script installs Git from source

# author:  Tukusej's Sirs
# date:    7 January 2020
# version: 1.3

# src: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git


# Install Git build and run dependencies
sudo yum -y install dh-autoreconf curl-devel expat-devel gettext-devel openssl-devel perl-devel zlib-devel asciidoc xmlto docbook2X getopt
sudo yum -y remove git

# We need to create this symlink due to binary name differences
sudo ln -s /usr/bin/db2x_docbook2texi /usr/bin/docbook2x-texi

# Clone Git repository
mkdir -p ${XDG_GIT_DIR}/git
git clone https://github.com/git/git ${XDG_GIT_DIR}/git
cd ${XDG_GIT_DIR}/git

# Configure Git
make configure
./configure --prefix=/usr

# Build and install Git (including docs)
# Note on `NO_SVN_TESTS=1`: I skip SVN tests because:
#   (1) I don’t use SVN nor `git-svn`;
#   (2) those tests take up a significant amount of the total test time and are not needed unless you plan to talk to SVN repos (according to the Git’s `Makefile`);
# Note on `GIT_SKIP_TESTS='t9020'`: On CentOS 8 some of these tests (t9020-remote-svn.sh: test 1, 2, 3, 4, 6) have failed me on 6 Jan 2020, so skipped the whole `t9020-remote-svn.sh` test altogether (again: because I don’t use `git-svn` at all).
# Note on `DEFAULT_EDITOR='/usr/bin/nano'`: I have set `nano` as the default editor for Git.
# Note on `profile`: This option makes `make` build Git much longer, but should make it ‘a few percent faster on CPU intensive workloads’ (according to Git’s `INSTALL`).
# Note: The following command took about 4 hours (while doing some other stuff) on Lenovo E531 with Intel(R) Core(TM) i5-3230M CPU @ 2.60GHz and 12 GB RAM (of which 2 GB is used by Intel graphics) using 3 cores (`-j3`)
NO_SVN_TESTS=1 GIT_SKIP_TESTS='t9020' DEFAULT_EDITOR="$(which nano)" make -j4 profile all doc info
# NO_SVN_TESTS=1 GIT_SKIP_TESTS='*svn*' DEFAULT_EDITOR='/usr/bin/nano' make -j4 profile all doc info
sudo bash -c "cd /git/git; NO_SVN_TESTS=1 GIT_SKIP_TESTS='t9020' DEFAULT_EDITOR=\"$(which nano)\" make PROFILE=BUILD install install-doc install-html install-info"

# TODO: if `make prefix=/opt profile` won't complete successfully, `git` should be build with the following two commands
# make prefix=/opt profile-fast
# sudo make prefix=/opt PROFILE=BUILD install