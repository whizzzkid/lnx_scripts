# This script installs Adobe Flash Player

# author:  Tukusej's Sirs
# date:    29 August 2019
# version: 1.0


sudo rpm -ivh http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
sudo rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
sudo yum -y install flash-plugin alsa-plugins-pulseaudio libcurl