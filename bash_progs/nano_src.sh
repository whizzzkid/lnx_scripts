#!/bin/bash

# This script installs `nano` from source

# author:  Tukusej's Sirs
# date:    2 September 2019
# version: 1.1

# dependecies: Requires `git_src` and `automake_src` to be installed in advance


sudo yum -y group install "Development Tools"
sudo yum -y install gettext-* git groff texinfo gcc-c++-x86_64-linux-gnu.x86_64
# - autoconf    (version >= 2.69)    # group: Development Tools
# - automake    (version >= 1.14)    # repo: okay
# - autopoint   (version >= 0.18.3)  # provided by `gettext{,-devel}`
# - gettext     (version >= 0.18.3)  # repo: base
# - git         (version >= 2.7.4)   # ! old (check: https://computingforgeeks.com/how-to-install-latest-version-of-git-git-2-x-on-centos-7/)
										#(build: https://gist.github.com/egorsmkv/30faa3e61c185a41e89cf849737d4d4b)
# - groff       (version >= 1.12)    # repo: base
# - pkg-config  (version >= 0.22)    # repo: base
# - texinfo     (version >= 4.0)     # repo: base
# - gcc         (any version)        # repo: updates
# - make        (any version)        # repo: base

# Install automake
# rpm -ivh http://repo.okay.com.mx/centos/7/x86_64/release/okay-release-1-1.noarch.rpm
# sudo yum -y install automake

mkdir -p ${XDG_GIT_DIR}/nano
git clone git://git.sv.gnu.org/nano.git ${XDG_GIT_DIR}/nano
cd ${XDG_GIT_DIR}/nano
./autogen.sh
./configure
make && sudo make install