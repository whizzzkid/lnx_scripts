#!/bin/bash

# This script installs MuseScore

# author:  Tukusej's Sirs
# date:    2 September 2019
# version: 1.1


mkdir -p ${XDG_PROG_BIN_DIR}/mscore
wget -qO ${XDG_PROG_BIN_DIR}/mscore/musescore-x86_64.appimage "$(curl -s "https://musescore.org/sk/download/musescore-x86_64.AppImage" | grep -Po "Please use this <a href=\"\K[^\"]*")"
chmod a+x ${XDG_PROG_BIN_DIR}/mscore/musescore-x86_64.appimage